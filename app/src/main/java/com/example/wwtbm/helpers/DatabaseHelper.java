package com.example.wwtbm.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;


public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "app.db";

    private static final String TABLE_NAME_USERS = "users_table";
    private static final String TABLE_NAME_RECORDS = "records_table";
    private static final String TABLE_NAME_QUESTIONS = "questions_table";

    private static final String CREATE_TABLE_USERS = "create table " + TABLE_NAME_USERS + " (id INTEGER PRIMARY KEY AUTOINCREMENT, userName TEXT, walletBalance INTEGER, gamesCompleted INTEGER, gamesCompletedWithoutHints INTEGER, maxRewardCount INTEGER, longestQuestionSeriesWithoutHintsWithinOneGame INTEGER, amountX2Used INTEGER, amountFiftyFiftyUsed INTEGER, amountCallUsed INTEGER, amountAudienceUsed INTEGER, levelNumber INTEGER, points INTEGER)";
    private static final String CREATE_TABLE_RECORDS = "create table " + TABLE_NAME_RECORDS + " (id INTEGER PRIMARY KEY AUTOINCREMENT,userName TEXT, gameID INTEGER, gameDate TEXT, gamePrizeAmount INTEGER, amountOfHintsUsed INTEGER, stableSumm INTEGER,x2Used INTEGER, fiftyFiftyUsed INTEGER, callUsed INTEGER, audienceUsed INTEGER, questionNumber INTEGER, lastQuestionText TEXT, lastQuestionAnswerGivenOrNot INTEGER, userAnswer TEXT, correctAnswer TEXT)";
    private static final String CREATE_TABLE_QUESTIONS = "create table " + TABLE_NAME_QUESTIONS + " (id INTEGER PRIMARY KEY AUTOINCREMENT, questionText TEXT, questionCorrectAnswer TEXT, varOne TEXT, varTwo TEXT, varThree TEXT, varFour TEXT, questionDifficultyLvl INTEGER, isUsed INTEGER)";


    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_USERS);
        db.execSQL(CREATE_TABLE_RECORDS);
        db.execSQL(CREATE_TABLE_QUESTIONS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean deleteUser(String currentUserName){
        SQLiteDatabase db = this.getWritableDatabase();

        String whereClause = "userName = ?";
        String[] whereArgs = new String[] {currentUserName};

        long result = db.delete(TABLE_NAME_USERS, whereClause, whereArgs);

        return !(result == -1);

    }

    public void deleteGamesFromHistory(){
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DELETE FROM " + TABLE_NAME_RECORDS);
    }


    public boolean insertToGameHistory(String userName, int gameID, String gameDate, int gamePrizeAmount,int amountOfHintsUsed, int stableSumm, int x2Used,int fiftyFiftyUsed,int callUsed,int audienceUsed, int questionNumber, String lastQuestionText, int lastQuestionAnswerGivenOrNot, String userAnswer, String correctAnswer) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("userName", userName);
        contentValues.put("gameID", gameID);
        contentValues.put("gameDate", gameDate);
        contentValues.put("gamePrizeAmount", gamePrizeAmount);
        contentValues.put("amountOfHintsUsed", amountOfHintsUsed);
        contentValues.put("stableSumm",stableSumm);
        contentValues.put("x2Used",x2Used);
        contentValues.put("fiftyFiftyUsed",fiftyFiftyUsed);
        contentValues.put("callUsed",callUsed);
        contentValues.put("audienceUsed",audienceUsed);
        contentValues.put("questionNumber",questionNumber);
        contentValues.put("lastQuestionText", lastQuestionText);
        contentValues.put("lastQuestionAnswerGivenOrNot", lastQuestionAnswerGivenOrNot);
        contentValues.put("userAnswer", userAnswer);
        contentValues.put("correctAnswer", correctAnswer);

        long result = db.insert(TABLE_NAME_RECORDS, null, contentValues);

        return !(result == -1);
    }

    public boolean insertUser(String userName, int walletBalance, int gamesCompleted, int gamesCompletedWithoutHints,int maxRewardCount,int longestQuestionSeriesWithoutHintsWithinOneGame,int amountX2Used,int amountFiftyFiftyUsed,int amountCallUsed, int amountAudienceUsed, int levelNumber, int points) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("userName", userName);
        contentValues.put("walletBalance", walletBalance);
        contentValues.put("gamesCompleted", gamesCompleted);
        contentValues.put("gamesCompletedWithoutHints", gamesCompletedWithoutHints);
        contentValues.put("maxRewardCount", maxRewardCount);
        contentValues.put("longestQuestionSeriesWithoutHintsWithinOneGame",longestQuestionSeriesWithoutHintsWithinOneGame);
        contentValues.put("amountX2Used",amountX2Used);
        contentValues.put("amountFiftyFiftyUsed",amountFiftyFiftyUsed);
        contentValues.put("amountCallUsed",amountCallUsed);
        contentValues.put("amountAudienceUsed",amountAudienceUsed);
        contentValues.put("levelNumber", levelNumber);
        contentValues.put("points", points);

        long result = db.insert(TABLE_NAME_USERS, null, contentValues);

        return !(result == -1);
    }


   public Cursor getGameWithTheHighestPrizeAmount(){
       SQLiteDatabase db = this.getWritableDatabase();
       return db.rawQuery("select * from records_table where gamePrizeAmount = (select MAX(gamePrizeAmount) from records_table limit 1)",null);
   }

    public boolean insertQuestion(String questionText, String questionCorrectAnswer, String varOne, String varTwo, String varThree, String varFour, int questionDifficultyLvl) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("questionText", questionText);
        contentValues.put("questionCorrectAnswer", questionCorrectAnswer);
        contentValues.put("varOne", varOne);
        contentValues.put("varTwo", varTwo);
        contentValues.put("varThree", varThree);
        contentValues.put("varFour", varFour);
        contentValues.put("questionDifficultyLvl", questionDifficultyLvl);
        contentValues.put("isUsed",0);

        long result = db.insert(TABLE_NAME_QUESTIONS, null, contentValues);

        return !(result == -1);
    }


    public boolean updateGamesCompletedWithoutHints(String userName, int gamesCompletedWithoutHints){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put("gamesCompletedWithoutHints",gamesCompletedWithoutHints);
        String whereClause = "userName = ?";
        String[] whereArgs = new String[] {userName};
        long result = db.update(TABLE_NAME_USERS,newValues,whereClause,whereArgs);
        return !(result == -1);
    }

    public boolean updateMaxRewardCount(String userName, int maxRewardCount){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put("maxRewardCount",maxRewardCount);
        String whereClause = "userName = ?";
        String[] whereArgs = new String[] {userName};
        long result = db.update(TABLE_NAME_USERS,newValues,whereClause,whereArgs);
        return !(result == -1);
    }

    public boolean updateLongestQuestionSeriesWithoutHintsWithinOneGame(String userName, int longestQuestionSeriesWithoutHintsWithinOneGame){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put("longestQuestionSeriesWithoutHintsWithinOneGame",longestQuestionSeriesWithoutHintsWithinOneGame);
        String whereClause = "userName = ?";
        String[] whereArgs = new String[] {userName};
        long result = db.update(TABLE_NAME_USERS,newValues,whereClause,whereArgs);
        return !(result == -1);
    }

    public boolean updateAmountX2Used(String userName, int amountX2Used){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put("amountX2Used",amountX2Used);
        String whereClause = "userName = ?";
        String[] whereArgs = new String[] {userName};
        long result = db.update(TABLE_NAME_USERS,newValues,whereClause,whereArgs);
        return !(result == -1);
    }

    public boolean updateAmountFiftyFiftyUsed(String userName, int amountFiftyFiftyUsed){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put("amountFiftyFiftyUsed",amountFiftyFiftyUsed);
        String whereClause = "userName = ?";
        String[] whereArgs = new String[] {userName};
        long result = db.update(TABLE_NAME_USERS,newValues,whereClause,whereArgs);
        return !(result == -1);
    }


    public boolean updateAmountCallUsed(String userName, int amountCallUsed){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put("amountCallUsed",amountCallUsed);
        String whereClause = "userName = ?";
        String[] whereArgs = new String[] {userName};
        long result = db.update(TABLE_NAME_USERS,newValues,whereClause,whereArgs);
        return !(result == -1);
    }

    public boolean updateAmountAudienceUsed(String userName, int amountAudienceUsed){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put("amountAudienceUsed",amountAudienceUsed);
        String whereClause = "userName = ?";
        String[] whereArgs = new String[] {userName};
        long result = db.update(TABLE_NAME_USERS,newValues,whereClause,whereArgs);
        return !(result == -1);
    }


    public boolean updateUsername(String newUserName){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put("userName", newUserName);
        String whereClause = "id = ?";
        String[] whereArgs = new String[] {String.valueOf(1)};
        long result = db.update(TABLE_NAME_USERS,newValues,whereClause,whereArgs);
        return !(result == -1);
    }

    public boolean updateLevelNumber(String userName, int levelNumber){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put("levelNumber",levelNumber);
        String whereClause = "userName = ?";
        String[] whereArgs = new String[] {userName};
        long result = db.update(TABLE_NAME_USERS,newValues,whereClause,whereArgs);
        return !(result == -1);
    }

    public boolean updatePoints(String userName, int points){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put("points",points);
        String whereClause = "userName = ?";
        String[] whereArgs = new String[] {userName};
        long result = db.update(TABLE_NAME_USERS,newValues,whereClause,whereArgs);
        return !(result == -1);
    }

    public boolean updateGamesCompleted(String userName, int gamesCompleted){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put("gamesCompleted",gamesCompleted);
        String whereClause = "userName = ?";
        String[] whereArgs = new String[] {userName};
        long result = db.update(TABLE_NAME_USERS,newValues,whereClause,whereArgs);
        return !(result == -1);
    }

    public boolean updateWalletBalance(String userName, int walletBalance){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put("walletBalance",walletBalance);
        String whereClause = "userName = ?";
        String[] whereArgs = new String[] {userName};
        long result = db.update(TABLE_NAME_USERS,newValues,whereClause,whereArgs);
        return !(result == -1);

    }

    public boolean changeIsUsed(String questionText){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put("isUsed",1);
        String whereClause = "questionText = ?";
        String[] whereArgs = new String[] {questionText};
        long result = db.update(TABLE_NAME_QUESTIONS,newValues,whereClause,whereArgs);
        return !(result == -1);

    }


    public Cursor getUniqueQuestionWithDiffLvl(int diffLvl){
        SQLiteDatabase db = this.getWritableDatabase();
        String[] selectionArgs = {String.valueOf(0),String.valueOf(diffLvl)};
        return db.rawQuery("select * from questions_table where (isUsed=? and questionDifficultyLvl=?)",selectionArgs);
    }



    public Cursor getUserInfo(){
        SQLiteDatabase db = this.getWritableDatabase();
        String[] selectionArgs = {};

        return db.rawQuery("select * from users_table",selectionArgs);
    }

}
