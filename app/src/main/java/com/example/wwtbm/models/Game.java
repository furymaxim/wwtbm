package com.example.wwtbm.models;

import java.util.Map;

public class Game {
    private int gameID, is5050Used, isX2Used, isCallToFriendUsed, isAudienceHintUsed, currentQuestionNumber, currentMoneyAmountEarned, stableSumm;
    private User user;
    private Map<Integer, Question> questionsInGame;


    public Game(User user,int gameID, int is5050Used, int isX2Used, int isCallToFriendUsed, int isAudienceHintUsed, int currentQuestionNumber, int currentMoneyAmountEarned, int stableSumm, Map<Integer, Question> questionsInGame) {
        this.user = user;
        this.gameID = gameID;
        this.is5050Used = is5050Used;
        this.isX2Used = isX2Used;
        this.isCallToFriendUsed = isCallToFriendUsed;
        this.isAudienceHintUsed = isAudienceHintUsed;
        this.currentQuestionNumber = currentQuestionNumber;
        this.currentMoneyAmountEarned = currentMoneyAmountEarned;
        this.stableSumm = stableSumm;
        this.questionsInGame = questionsInGame;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getGameID() {
        return gameID;
    }

    public void setGameID(int gameID) {
        this.gameID = gameID;
    }

    public int getIs5050Used() {
        return is5050Used;
    }

    public void setIs5050Used(int is5050Used) {
        this.is5050Used = is5050Used;
    }

    public int getIsX2Used() {
        return isX2Used;
    }

    public void setIsX2Used(int isX2Used) {
        this.isX2Used = isX2Used;
    }

    public int getIsCallToFriendUsed() {
        return isCallToFriendUsed;
    }

    public void setIsCallToFriendUsed(int isCallToFriendUsed) {
        this.isCallToFriendUsed = isCallToFriendUsed;
    }

    public int getIsAudienceHintUsed() {
        return isAudienceHintUsed;
    }

    public void setIsAudienceHintUsed(int isAudienceHintUsed) {
        this.isAudienceHintUsed = isAudienceHintUsed;
    }

    public int getCurrentQuestionNumber() {
        return currentQuestionNumber;
    }

    public void setCurrentQuestionNumber(int currentQuestionNumber) {
        this.currentQuestionNumber = currentQuestionNumber;
    }

    public int getCurrentMoneyAmountEarned() {
        return currentMoneyAmountEarned;
    }

    public void setCurrentMoneyAmountEarned(int currentMoneyAmountEarned) {
        this.currentMoneyAmountEarned = currentMoneyAmountEarned;
    }

    public int getStableSumm() {
        return stableSumm;
    }

    public void setStableSumm(int stableSumm) {
        this.stableSumm = stableSumm;
    }

    public Map<Integer, Question> getQuestionsInGame() {
        return questionsInGame;
    }

    public void setQuestionsInGame(Map<Integer, Question> questionsInGame) {
        this.questionsInGame = questionsInGame;
    }
}
