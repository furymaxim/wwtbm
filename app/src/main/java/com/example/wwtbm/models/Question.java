package com.example.wwtbm.models;


public class Question {

    private String questionText;
    private String questionCorrectAnswer;
    private String varOne, varTwo, varThree, varFour;
    private int difficultyLvl, isUsed;

    public Question(String questionText,String questionCorrectAnswer, String varOne, String varTwo, String varThree, String varFour, int difficultyLvl){
        this.questionText = questionText;
        this.questionCorrectAnswer = questionCorrectAnswer;
        this.varOne = varOne;
        this.varTwo = varTwo;
        this.varThree = varThree;
        this.varFour = varFour;
        this.difficultyLvl = difficultyLvl;
        this.isUsed = 0;
    }


    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getQuestionCorrectAnswer() {
        return questionCorrectAnswer;
    }

    public void setQuestionCorrectAnswer(String questionCorrectAnswer) {
        this.questionCorrectAnswer = questionCorrectAnswer;
    }

    public String getVarOne() {
        return varOne;
    }

    public void setVarOne(String varOne) {
        this.varOne = varOne;
    }

    public String getVarTwo() {
        return varTwo;
    }

    public void setVarTwo(String varTwo) {
        this.varTwo = varTwo;
    }

    public String getVarThree() {
        return varThree;
    }

    public void setVarThree(String varThree) {
        this.varThree = varThree;
    }

    public String getVarFour() {
        return varFour;
    }

    public void setVarFour(String varFour) {
        this.varFour = varFour;
    }

    public int getDifficultyLvl() {
        return difficultyLvl;
    }

    public void setDifficultyLvl(int difficultyLvl) {
        this.difficultyLvl = difficultyLvl;
    }

    public int getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(int isUsed) {
        this.isUsed = isUsed;
    }
}
