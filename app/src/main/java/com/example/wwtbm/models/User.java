package com.example.wwtbm.models;

public class User {

    private String userName;
    private int walletBalance, gamesCompleted, gamesCompletedWithoutHints, maxRewardCount, levelNumber, points, longestQuestionSeriesWithoutHintsWithinOneGame, amountX2Used, amountFiftyFiftyUsed, amountCallUsed, amountAudienceUsed;

    public User(String userName, int walletBalance, int gamesCompleted, int gamesCompletedWithoutHints, int maxRewardCount, int longestQuestionSeriesWithoutHintsWithinOneGame, int amountX2Used, int amountFiftyFiftyUsed, int amountCallUsed, int amountAudienceUsed, int levelNumber, int points) {
        this.userName = userName;
        this.walletBalance = walletBalance;
        this.gamesCompleted = gamesCompleted;
        this.gamesCompletedWithoutHints = gamesCompletedWithoutHints;
        this.maxRewardCount = maxRewardCount;
        this.levelNumber = levelNumber;
        this.points = points;
        this.longestQuestionSeriesWithoutHintsWithinOneGame = longestQuestionSeriesWithoutHintsWithinOneGame;
        this.amountX2Used = amountX2Used;
        this.amountFiftyFiftyUsed = amountFiftyFiftyUsed;
        this.amountAudienceUsed = amountAudienceUsed;
        this.amountCallUsed = amountCallUsed;
    }

    public int getLongestQuestionSeriesWithoutHintsWithinOneGame() {
        return longestQuestionSeriesWithoutHintsWithinOneGame;
    }

    public void setLongestQuestionSeriesWithoutHintsWithinOneGame(int longestQuestionSeriesWithoutHintsWithinOneGame) {
        this.longestQuestionSeriesWithoutHintsWithinOneGame = longestQuestionSeriesWithoutHintsWithinOneGame;
    }

    public int getAmountX2Used() {
        return amountX2Used;
    }

    public void setAmountX2Used(int amountX2Used) {
        this.amountX2Used = amountX2Used;
    }

    public int getAmountFiftyFiftyUsed() {
        return amountFiftyFiftyUsed;
    }

    public void setAmountFiftyFiftyUsed(int mountFiftyFiftyUsed) {
        this.amountFiftyFiftyUsed = mountFiftyFiftyUsed;
    }

    public int getAmountCallUsed() {
        return amountCallUsed;
    }

    public void setAmountCallUsed(int amountCallUsed) {
        this.amountCallUsed = amountCallUsed;
    }

    public int getAmountAudienceUsed() {
        return amountAudienceUsed;
    }

    public void setAmountAudienceUsed(int amountAudienceUsed) {
        this.amountAudienceUsed = amountAudienceUsed;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(int walletBalance) {
        this.walletBalance = walletBalance;
    }

    public int getGamesCompleted() {
        return gamesCompleted;
    }

    public void setGamesCompleted(int gamesCompleted) {
        this.gamesCompleted = gamesCompleted;
    }

    public int getGamesCompletedWithoutHints() {
        return gamesCompletedWithoutHints;
    }

    public void setGamesCompletedWithoutHints(int gamesCompletedWithoutHints) {
        this.gamesCompletedWithoutHints = gamesCompletedWithoutHints;
    }

    public int getMaxRewardCount() {
        return maxRewardCount;
    }

    public void setMaxRewardCount(int maxRewardCount) {
        this.maxRewardCount = maxRewardCount;
    }

    public int getLevelNumber() {
        return levelNumber;
    }

    public void setLevelNumber(int levelNumber) {
        this.levelNumber = levelNumber;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
