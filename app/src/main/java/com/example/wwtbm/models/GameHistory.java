package com.example.wwtbm.models;

public class GameHistory {

    private int gameID;
    private String gameDate;
    private int gamePrizeAmount;
    private int amountOfHintsUsed;
    private int x2Used, fiftyFiftyUsed,callUsed, audienceUsed;
    private int questionNumber;
    private int stableSumm;
    private String lastQuestionText;
    private int lastQuestionAnswerGivenOrNot;
    private String userAnswer;
    private String correctAnswer;
    private String userName;




    public GameHistory(String userName, int gameID, String gameDate, int gamePrizeAmount, int amountOfHintsUsed, int stableSumm, int x2Used, int fiftyFiftyUsed, int callUsed, int audienceUsed, int questionNumber, String lastQuestionText, int lastQuestionAnswerGivenOrNot, String userAnswer, String correctAnswer) {
        this.gameID = gameID;
        this.gameDate = gameDate;
        this.gamePrizeAmount = gamePrizeAmount;
        this.amountOfHintsUsed = amountOfHintsUsed;
        this.x2Used = x2Used;
        this.fiftyFiftyUsed = fiftyFiftyUsed;
        this.callUsed = callUsed;
        this.audienceUsed = audienceUsed;
        this.questionNumber = questionNumber;
        this.lastQuestionText = lastQuestionText;
        this.lastQuestionAnswerGivenOrNot = lastQuestionAnswerGivenOrNot;
        this.userAnswer = userAnswer;
        this.correctAnswer = correctAnswer;
        this.userName = userName;
        this.stableSumm = stableSumm;
    }

    public int getStableSumm() {
        return stableSumm;
    }

    public void setStableSumm(int stableSumm) {
        this.stableSumm = stableSumm;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getGameID() {
        return gameID;
    }

    public void setGameID(int gameID) {
        this.gameID = gameID;
    }

    public String getGameDate() {
        return gameDate;
    }

    public void setGameDate(String gameDate) {
        this.gameDate = gameDate;
    }

    public int getGamePrizeAmount() {
        return gamePrizeAmount;
    }

    public void setGamePrizeAmount(int gamePrizeAmount) {
        this.gamePrizeAmount = gamePrizeAmount;
    }

    public int getAmountOfHintsUsed() {
        return amountOfHintsUsed;
    }

    public void setAmountOfHintsUsed(int amountOfHintsUsed) {
        this.amountOfHintsUsed = amountOfHintsUsed;
    }

    public int getX2Used() {
        return x2Used;
    }

    public void setX2Used(int x2Used) {
        this.x2Used = x2Used;
    }

    public int getFiftyFiftyUsed() {
        return fiftyFiftyUsed;
    }

    public void setFiftyFiftyUsed(int fiftyFiftyUsed) {
        this.fiftyFiftyUsed = fiftyFiftyUsed;
    }

    public int getCallUsed() {
        return callUsed;
    }

    public void setCallUsed(int callUsed) {
        this.callUsed = callUsed;
    }

    public int getAudienceUsed() {
        return audienceUsed;
    }

    public void setAudienceUsed(int audienceUsed) {
        this.audienceUsed = audienceUsed;
    }

    public int getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        this.questionNumber = questionNumber;
    }

    public String getLastQuestionText() {
        return lastQuestionText;
    }

    public void setLastQuestionText(String lastQuestionText) {
        this.lastQuestionText = lastQuestionText;
    }

    public int getLastQuestionAnswerGivenOrNot() {
        return lastQuestionAnswerGivenOrNot;
    }

    public void setLastQuestionAnswerGivenOrNot(int lastQuestionAnswerGivenOrNot) {
        this.lastQuestionAnswerGivenOrNot = lastQuestionAnswerGivenOrNot;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }
}
