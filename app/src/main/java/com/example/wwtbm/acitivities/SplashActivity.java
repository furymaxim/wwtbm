package com.example.wwtbm.acitivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ProgressBar;


import com.example.wwtbm.R;
import com.example.wwtbm.helpers.DatabaseHelper;
import com.example.wwtbm.helpers.JSONHelper;
import com.example.wwtbm.models.Question;

import org.json.JSONArray;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.List;

public class SplashActivity extends AppCompatActivity {

    private ProgressBar progressBar;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        progressBar = findViewById(R.id.splash_progress_bar);
        sharedPreferences = getSharedPreferences("splash", Context.MODE_PRIVATE);



        if(!sharedPreferences.contains("questionsLoaded")){
            insertToDB(loadQuestions());
            getSharedPreferences("splash",Context.MODE_PRIVATE).edit().putBoolean("questionsLoaded",true).apply();
        }

        startNewThread();

    }

    private void startNewThread() {

        new Thread(() -> {
            doWork();
            startApp();
            finish();
        }).start();
    }

    private void doWork(){
        Drawable draw = getDrawable(R.drawable.custom_progressbar);
        for (int progress=0; progress<100; progress+=10) {
            progressBar.setProgressDrawable(draw);
            if(progress<=60) {
                try {
                    Thread.sleep(500);
                    progressBar.setProgress(progress);
                    progressBar.setProgressDrawable(draw);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                try {
                    Thread.sleep(250);
                    progressBar.setProgress(progress);
                    progressBar.setProgressDrawable(draw);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void startApp() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private List<Question> loadQuestions(){
        List <Question> list = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(JSONHelper.loadJSONFromAsset(getApplicationContext()));

            JSONArray questions = (JSONArray) jsonObject.get("questions");

            for (int i = 0; i < questions.length(); i++) {
                JSONObject question = questions.getJSONObject(i);

                String questionText = question.getString("question_text");
                String questionCorrectAnswer = question.getString("question_correct_answer");
                String varOne = question.getString("var_one");
                String varTwo = question.getString("var_two");
                String varThree = question.getString("var_three");
                String varFour = question.getString("var_four");
                int difficultyLvl = question.getInt("difficulty_level");

                list.add(new Question(questionText, questionCorrectAnswer, varOne, varTwo, varThree, varFour, difficultyLvl));
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return list;
    }


    private void insertToDB(List<Question> list){
        Question[] array = new Question[list.size()];
        list.toArray(array);
        DatabaseHelper myDB = new DatabaseHelper(getApplicationContext());
        for(Question question: array){
            myDB.insertQuestion(question.getQuestionText(), question.getQuestionCorrectAnswer(), question.getVarOne(), question.getVarTwo(), question.getVarThree(), question.getVarFour(), question.getDifficultyLvl());
        }

    }



}

