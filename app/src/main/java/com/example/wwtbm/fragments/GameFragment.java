package com.example.wwtbm.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.example.wwtbm.R;
import com.example.wwtbm.helpers.DatabaseHelper;
import com.example.wwtbm.models.Game;
import com.example.wwtbm.models.Question;
import com.example.wwtbm.models.User;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import ticker.views.com.ticker.widgets.circular.timer.callbacks.CircularViewCallback;
import ticker.views.com.ticker.widgets.circular.timer.view.CircularView;

import static com.example.wwtbm.fragments.MenuFragment.myPreference;

public class GameFragment extends Fragment {

    private int questionsInRowWithoutHints = 0;
    private SharedPreferences sharedPreferences;
    private CircularView circularViewWithTimer;
    private String[] stableSumms = {"500", "1 000", "2 000", "5 000", "10 000", "25 000", "50 000", "100 000", "200 000", "400 000", "800 000", "1 500 000"};
    private Game game;
    private User user;
    private DatabaseHelper myDB;
    private Cursor c;
    private ImageView exit;
    private TextView questionText, var1, var2, var3, var4, fiftyFifty, x2, currentSumm;
    private TextView mark3QuestionNumber, mark3Rhombus, mark3Summ , mark1QuestionNumber, mark2QuestionNumber,mark4QuestionNumber, mark5QuestionNumber, mark6QuestionNumber, mark7QuestionNumber, mark8QuestionNumber, mark9QuestionNumber, mark10QuestionNumber, mark11QuestionNumber, mark12QuestionNumber, mark1Rhombus, mark2Rhombus, mark4Rhombus, mark5Rhombus, mark6Rhombus, mark7Rhombus, mark8Rhombus,
            mark9Rhombus, mark10Rhombus, mark11Rhombus, mark12Rhombus, mark1Summ, mark2Summ, mark4Summ, mark5Summ, mark6Summ, mark7Summ, mark8Summ, mark9Summ, mark10Summ, mark11Summ, mark12Summ;
    private LinearLayout callToFriend, audienceHelp;
    private ConstraintLayout mark1, mark2, mark3, mark4, mark5, mark6, mark7, mark8, mark9, mark10, mark11, mark12,mark13, takeCash;
    private TextView rhombusTv;
    private String formattedDate;


    GameFragment(User user){
        this.user = user;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_game, container, false);

        rhombusTv = view.findViewById(R.id.textView2);
        exit = view.findViewById(R.id.exit);
        questionText = view.findViewById(R.id.question_tv);
        var1 = view.findViewById(R.id.var1_tv);
        var2 = view.findViewById(R.id.var2_tv);
        var3 = view.findViewById(R.id.var3_tv);
        var4 = view.findViewById(R.id.var4_tv);
        fiftyFifty = view.findViewById(R.id.fifty_fifty);
        x2 = view.findViewById(R.id.x2);
        currentSumm = view.findViewById(R.id.current_summ);
        mark1QuestionNumber = view.findViewById(R.id.mark_1_question_number);
        mark2QuestionNumber = view.findViewById(R.id.mark_2_question_number);
        mark3QuestionNumber = view.findViewById(R.id.mark_3_question_number);
        mark4QuestionNumber = view.findViewById(R.id.mark_4_question_number);
        mark5QuestionNumber = view.findViewById(R.id.mark_5_question_number);
        mark6QuestionNumber = view.findViewById(R.id.mark_6_question_number);
        mark7QuestionNumber = view.findViewById(R.id.mark_7_question_number);
        mark8QuestionNumber = view.findViewById(R.id.mark_8_question_number);
        mark9QuestionNumber = view.findViewById(R.id.mark_9_question_number);
        mark10QuestionNumber = view.findViewById(R.id.mark_10_question_number);
        mark11QuestionNumber = view.findViewById(R.id.mark_11_question_number);
        mark12QuestionNumber = view.findViewById(R.id.mark_12_question_number);
        mark1Rhombus = view.findViewById(R.id.rhombus_1);
        mark2Rhombus = view.findViewById(R.id.rhombus_2);
        mark3Rhombus = view.findViewById(R.id.rhombus_3);
        mark4Rhombus = view.findViewById(R.id.rhombus_4);
        mark5Rhombus = view.findViewById(R.id.rhombus_5);
        mark6Rhombus = view.findViewById(R.id.rhombus_6);
        mark7Rhombus = view.findViewById(R.id.rhombus_7);
        mark8Rhombus = view.findViewById(R.id.rhombus_8);
        mark9Rhombus = view.findViewById(R.id.rhombus_9);
        mark10Rhombus = view.findViewById(R.id.rhombus_10);
        mark11Rhombus = view.findViewById(R.id.rhombus_11);
        mark12Rhombus = view.findViewById(R.id.rhombus_12);
        mark1Summ = view.findViewById(R.id.summ_1);
        mark2Summ = view.findViewById(R.id.summ_2);
        mark3Summ = view.findViewById(R.id.summ_3);
        mark4Summ = view.findViewById(R.id.summ_4);
        mark5Summ = view.findViewById(R.id.summ_5);
        mark6Summ = view.findViewById(R.id.summ_6);
        mark7Summ = view.findViewById(R.id.summ_7);
        mark8Summ = view.findViewById(R.id.summ_8);
        mark9Summ = view.findViewById(R.id.summ_9);
        mark10Summ = view.findViewById(R.id.summ_10);
        mark11Summ = view.findViewById(R.id.summ_11);
        mark12Summ = view.findViewById(R.id.summ_12);
        callToFriend = view.findViewById(R.id.call_to_friend);
        audienceHelp = view.findViewById(R.id.audience_help);
        mark1 = view.findViewById(R.id.mark_1);
        mark2 = view.findViewById(R.id.mark_2);
        mark3 = view.findViewById(R.id.mark_3);
        mark4 = view.findViewById(R.id.mark_4);
        mark5 = view.findViewById(R.id.mark_5);
        mark6 = view.findViewById(R.id.mark_6);
        mark7 = view.findViewById(R.id.mark_7);
        mark8 = view.findViewById(R.id.mark_8);
        mark9 = view.findViewById(R.id.mark_9);
        mark10 = view.findViewById(R.id.mark_10);
        mark11 = view.findViewById(R.id.mark_11);
        mark12 = view.findViewById(R.id.mark_12);
        mark13 = view.findViewById(R.id.mark_13);
        takeCash = view.findViewById(R.id.take_cash);

        circularViewWithTimer = view.findViewById(R.id.circular_view_with_timer);

        sharedPreferences = getActivity().getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        dialogStableSumm();

        exit.setOnClickListener((v) -> dialogProgressLost());

        return view;
    }


    private void getBackWithResult(){
        circularViewWithTimer.stopTimer();
        backToMenu();
    }

    private void dialogStableSumm() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(false);
        final View mView = getLayoutInflater().inflate(R.layout.dialog_stable_summ, null);
        final Spinner spinner = mView.findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item, stableSumms);
        adapter.setDropDownViewResource(R.layout.spinner_drop_down_item);
        spinner.setSelection(1);
        spinner.setAdapter(adapter);
        alertDialog.setView(mView);
        alertDialog.setPositiveButton("Начать игру!", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                sharedPreferences.edit().putString("stableSumm", spinner.getSelectedItem().toString()).apply();
                game = initGame();
                startGame(game);
                dialog.dismiss();
            }

        });

        AlertDialog dialog = alertDialog.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }




    private void startGame(Game game) {
        renderView(game);
        launchTimer();

        sharedPreferences.edit().remove("x2CurrentlyUsing").apply();

        if (sharedPreferences.getBoolean("isTimerRunning", true)) {
            if(game.getCurrentQuestionNumber() != 1) {
                takeCash.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogGetMoney();
                    }
                });
            }

            callToFriend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sharedPreferences.contains("x2CurrentlyUsing") && sharedPreferences.getBoolean("x2CurrentlyUsing", true)) {
                        Toast.makeText(getContext(), "Использование подсказки невозможно в настоящий момент", Toast.LENGTH_SHORT).show();
                    } else {
                        questionsInRowWithoutHints = -1;
                        if (game.getIsX2Used() == 0 || sharedPreferences.contains("isX2usedInLastRound")) {
                            String[] questionVars = {game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarOne(), game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarTwo(), game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarThree(), game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarFour()};
                            int emptyCount = 0;
                            for (int i = 0; i < questionVars.length; i++) {
                                if (questionVars[i].equals("")) {
                                    emptyCount++;
                                }
                            }
                            if (emptyCount != 3) {
                                String correctAnswer = game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer();
                                int correctAnswerPosition = 0;
                                for (int i = 0; i < questionVars.length; i++) {
                                    if (questionVars[i].equals(correctAnswer)) {
                                        correctAnswerPosition = i;
                                    }
                                }

                                Random r = new Random();
                                int low = 1;
                                int high = 100;
                                int result = r.nextInt(high - low) + low;
                                if (result >= 60) {
                                    dialogFriendHelp(correctAnswer);
                                } else {
                                    String[] varsWithNoCorrect = {"", "", ""};
                                    for (int i = 0, k = 0; i < questionVars.length; i++) {
                                        if (i == correctAnswerPosition) {
                                            continue;
                                        }
                                        varsWithNoCorrect[k++] = questionVars[i];
                                    }

                                    String notEmptyVar;
                                    do {
                                        notEmptyVar = varsWithNoCorrect[(int) (System.currentTimeMillis() % varsWithNoCorrect.length)];
                                    } while (notEmptyVar.equals(""));
                                    dialogFriendHelp(notEmptyVar);
                                }
                                game.setIsCallToFriendUsed(1);
                                callToFriend.setVisibility(View.GONE);
                            } else {
                                Toast.makeText(getContext(), "Незачем беспокоить друга при одном варианте ответа.", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getContext(), "Использование подсказки невозможно в настоящий момент", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

            audienceHelp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sharedPreferences.contains("x2CurrentlyUsing") && sharedPreferences.getBoolean("x2CurrentlyUsing", true)) {
                        Toast.makeText(getContext(), "Использование подсказки невозможно в настоящий момент", Toast.LENGTH_SHORT).show();
                    } else {
                        questionsInRowWithoutHints = -1;
                        if (game.getIsX2Used() == 0 || sharedPreferences.contains("isX2usedInLastRound")) {
                            String[] questionVars = {game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarOne(), game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarTwo(), game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarThree(), game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarFour()};
                            String correctAnswer = game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer();
                            int correctAnswerPosition = 0;
                            for (int i = 0; i < questionVars.length; i++) {
                                if (questionVars[i].equals(correctAnswer)) {
                                    correctAnswerPosition = i;
                                }
                            }

                            Random r = new Random();
                            int low = 1;
                            int high = 100;
                            int result = r.nextInt(high - low) + low;

                            dialogAudienceHelp(questionVars, giveResults(result,questionVars, correctAnswerPosition));

                            game.setIsAudienceHintUsed(1);
                            audienceHelp.setVisibility(View.GONE);
                        } else {
                            Toast.makeText(getContext(), "Использование подсказки невозможно в настоящий момент", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

            fiftyFifty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sharedPreferences.contains("x2CurrentlyUsing") && sharedPreferences.getBoolean("x2CurrentlyUsing", true)) {
                        Toast.makeText(getContext(), "Использование подсказки невозможно в настоящий момент", Toast.LENGTH_SHORT).show();
                    } else {
                        questionsInRowWithoutHints = -1;
                        if (game.getIsX2Used() == 0 || sharedPreferences.contains("isX2usedInLastRound")) {
                            String[] questionVars = {game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarOne(), game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarTwo(), game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarThree(), game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarFour()};
                            String correctAnswer = game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer();
                            int correctAnswerPosition = 0;
                            int low, high;
                            for (int i = 0; i < questionVars.length; i++) {
                                if (questionVars[i].equals(correctAnswer)) {
                                    correctAnswerPosition = i;
                                }
                            }

                            int toDeleteOne, toDeleteTwo;

                            Random r = new Random();
                            low = 1;
                            high = 4;
                            do {
                                toDeleteOne = r.nextInt(high - low) + low;
                            } while (toDeleteOne == correctAnswerPosition + 1);

                            do {
                                toDeleteTwo = r.nextInt(high - low) + low;
                            } while (toDeleteTwo == correctAnswerPosition + 1 || toDeleteTwo == toDeleteOne);

                            switch (toDeleteOne) {
                                case 1:
                                    game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).setVarOne("");
                                    var1.setOnClickListener(null);
                                    var1.setText("");
                                    break;
                                case 2:
                                    game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).setVarTwo("");
                                    var2.setOnClickListener(null);
                                    var2.setText("");
                                    break;
                                case 3:
                                    game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).setVarThree("");
                                    var3.setOnClickListener(null);
                                    var3.setText("");
                                    break;
                                case 4:
                                    game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).setVarFour("");
                                    var4.setOnClickListener(null);
                                    var4.setText("");
                                    break;
                            }

                            switch (toDeleteTwo) {
                                case 1:
                                    game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).setVarOne("");
                                    var1.setOnClickListener(null);
                                    var1.setText("");
                                    break;
                                case 2:
                                    game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).setVarTwo("");
                                    var2.setOnClickListener(null);
                                    var2.setText("");
                                    break;
                                case 3:
                                    game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).setVarThree("");
                                    var3.setOnClickListener(null);
                                    var3.setText("");
                                    break;
                                case 4:
                                    game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).setVarFour("");
                                    var4.setOnClickListener(null);
                                    var4.setText("");
                                    break;
                            }


                            game.setIs5050Used(1);
                            fiftyFifty.setVisibility(View.GONE);
                        } else {
                            Toast.makeText(getContext(), "Использование подсказки невозможно в настоящий момент", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

            x2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    questionsInRowWithoutHints = -1;
                    sharedPreferences.edit().putBoolean("x2CurrentlyUsing", true).apply();
                    rhombusTv.setText("X2");
                    game.setIsX2Used(1);
                    x2.setVisibility(View.GONE);
                }
            });

            var1.setOnClickListener((View view)->{
                    if (game.getIsX2Used() == 0 || sharedPreferences.contains("isX2usedInLastRound")) {
                        circularViewWithTimer.stopTimer();
                        if (((game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarOne()).equals(game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer()))) {
                            questionsInRowWithoutHints = questionsInRowWithoutHints + 1;
                            if(questionsInRowWithoutHints > user.getLongestQuestionSeriesWithoutHintsWithinOneGame()) {
                                myDB.updateLongestQuestionSeriesWithoutHintsWithinOneGame(user.getUserName(), questionsInRowWithoutHints);
                                user.setLongestQuestionSeriesWithoutHintsWithinOneGame(questionsInRowWithoutHints);
                            }
                            if(game.getCurrentQuestionNumber() == 13){
                                game.setCurrentMoneyAmountEarned(getCurrentQuestionPrize(game.getCurrentQuestionNumber()));
                                takeCash.setVisibility(View.INVISIBLE);
                                takeCash.setOnClickListener(null);
                                dialogYouWon();
                            }else {
                                game.setCurrentMoneyAmountEarned(getCurrentQuestionPrize(game.getCurrentQuestionNumber()));
                                game.setCurrentQuestionNumber(game.getCurrentQuestionNumber() + 1);
                                startGame(game);
                            }
                        } else {
                            dialogYouLost(1);
                        }
                    } else if (game.getIsX2Used() == 1){
                        if (((game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarOne()).equals(game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer()))) {
                            if(questionsInRowWithoutHints > user.getLongestQuestionSeriesWithoutHintsWithinOneGame()) {
                                myDB.updateLongestQuestionSeriesWithoutHintsWithinOneGame(user.getUserName(), questionsInRowWithoutHints);
                                user.setLongestQuestionSeriesWithoutHintsWithinOneGame(questionsInRowWithoutHints);
                            }
                            questionsInRowWithoutHints = 0;
                            sharedPreferences.edit().putBoolean("isX2usedInLastRound",true).apply();
                            rhombusTv.setText("");
                            if(game.getCurrentQuestionNumber() == 13){
                                game.setCurrentMoneyAmountEarned(getCurrentQuestionPrize(game.getCurrentQuestionNumber()));
                                takeCash.setVisibility(View.INVISIBLE);
                                takeCash.setOnClickListener(null);
                                dialogYouWon();
                            }else{
                                sharedPreferences.edit().putBoolean("x2CurrentlyUsing", false).apply();
                                game.setCurrentMoneyAmountEarned(getCurrentQuestionPrize(game.getCurrentQuestionNumber()));
                                game.setCurrentQuestionNumber(game.getCurrentQuestionNumber() + 1);
                                startGame(game);
                            }
                        }else{
                            rhombusTv.setText("");
                            var1.setOnClickListener(null);
                            var1.setText("");
                            game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).setVarOne("");
                            sharedPreferences.edit().putBoolean("isX2usedInLastRound",true).apply();
                        }
                    }

            });

            var2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (game.getIsX2Used() == 0 || sharedPreferences.contains("isX2usedInLastRound")) {
                        circularViewWithTimer.stopTimer();
                        if (((game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarTwo()).equals(game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer()))) {
                            questionsInRowWithoutHints = questionsInRowWithoutHints + 1;
                            if(questionsInRowWithoutHints > user.getLongestQuestionSeriesWithoutHintsWithinOneGame()) {
                                myDB.updateLongestQuestionSeriesWithoutHintsWithinOneGame(user.getUserName(), questionsInRowWithoutHints);
                                user.setLongestQuestionSeriesWithoutHintsWithinOneGame(questionsInRowWithoutHints);
                            }
                            if(game.getCurrentQuestionNumber() == 13){
                                game.setCurrentMoneyAmountEarned(getCurrentQuestionPrize(game.getCurrentQuestionNumber()));
                                takeCash.setVisibility(View.INVISIBLE);
                                takeCash.setOnClickListener(null);
                                dialogYouWon();
                            }else {
                                game.setCurrentMoneyAmountEarned(getCurrentQuestionPrize(game.getCurrentQuestionNumber()));
                                game.setCurrentQuestionNumber(game.getCurrentQuestionNumber() + 1);
                                startGame(game);
                            }
                        } else {
                            dialogYouLost(2);
                        }
                    } else if (game.getIsX2Used() == 1){
                        if (((game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarTwo()).equals(game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer()))) {
                            if(questionsInRowWithoutHints > user.getLongestQuestionSeriesWithoutHintsWithinOneGame()) {
                                myDB.updateLongestQuestionSeriesWithoutHintsWithinOneGame(user.getUserName(), questionsInRowWithoutHints);
                                user.setLongestQuestionSeriesWithoutHintsWithinOneGame(questionsInRowWithoutHints);
                            }
                            questionsInRowWithoutHints = 0;
                            sharedPreferences.edit().putBoolean("isX2usedInLastRound",true).apply();
                            rhombusTv.setText("");
                            if(game.getCurrentQuestionNumber() == 13){
                                game.setCurrentMoneyAmountEarned(getCurrentQuestionPrize(game.getCurrentQuestionNumber()));
                                takeCash.setVisibility(View.INVISIBLE);
                                takeCash.setOnClickListener(null);
                                dialogYouWon();
                            }else{
                                sharedPreferences.edit().putBoolean("x2CurrentlyUsing", false).apply();
                                game.setCurrentMoneyAmountEarned(getCurrentQuestionPrize(game.getCurrentQuestionNumber()));
                                game.setCurrentQuestionNumber(game.getCurrentQuestionNumber() + 1);
                                startGame(game);
                            }
                        }else{
                            rhombusTv.setText("");
                            var2.setOnClickListener(null);
                            var2.setText("");
                            game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).setVarTwo("");
                            sharedPreferences.edit().putBoolean("isX2usedInLastRound",true).apply();
                        }

                    }
                }
            });

            var3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (game.getIsX2Used() == 0 || sharedPreferences.contains("isX2usedInLastRound")) {
                        circularViewWithTimer.stopTimer();
                        if (((game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarThree()).equals(game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer()))) {
                            questionsInRowWithoutHints = questionsInRowWithoutHints + 1;
                            if(questionsInRowWithoutHints > user.getLongestQuestionSeriesWithoutHintsWithinOneGame()) {
                                myDB.updateLongestQuestionSeriesWithoutHintsWithinOneGame(user.getUserName(), questionsInRowWithoutHints);
                                user.setLongestQuestionSeriesWithoutHintsWithinOneGame(questionsInRowWithoutHints);
                            }
                            if(game.getCurrentQuestionNumber() == 13){
                                game.setCurrentMoneyAmountEarned(getCurrentQuestionPrize(game.getCurrentQuestionNumber()));
                                takeCash.setVisibility(View.INVISIBLE);
                                takeCash.setOnClickListener(null);
                                dialogYouWon();
                            }else {
                                game.setCurrentMoneyAmountEarned(getCurrentQuestionPrize(game.getCurrentQuestionNumber()));
                                game.setCurrentQuestionNumber(game.getCurrentQuestionNumber() + 1);
                                startGame(game);
                            }
                        } else {
                            dialogYouLost(3);
                        }
                    } else if (game.getIsX2Used() == 1){
                        if (((game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarThree()).equals(game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer()))) {
                            if(questionsInRowWithoutHints > user.getLongestQuestionSeriesWithoutHintsWithinOneGame()) {
                                myDB.updateLongestQuestionSeriesWithoutHintsWithinOneGame(user.getUserName(), questionsInRowWithoutHints);
                                user.setLongestQuestionSeriesWithoutHintsWithinOneGame(questionsInRowWithoutHints);
                            }
                            questionsInRowWithoutHints = 0;
                            sharedPreferences.edit().putBoolean("isX2usedInLastRound",true).apply();
                            rhombusTv.setText("");
                            if(game.getCurrentQuestionNumber() == 13){
                                game.setCurrentMoneyAmountEarned(getCurrentQuestionPrize(game.getCurrentQuestionNumber()));
                                takeCash.setVisibility(View.INVISIBLE);
                                takeCash.setOnClickListener(null);
                                dialogYouWon();
                            }else {
                                sharedPreferences.edit().putBoolean("x2CurrentlyUsing", false).apply();
                                game.setCurrentMoneyAmountEarned(getCurrentQuestionPrize(game.getCurrentQuestionNumber()));
                                game.setCurrentQuestionNumber(game.getCurrentQuestionNumber() + 1);
                                startGame(game);
                            }
                        }else{
                            rhombusTv.setText("");
                            var3.setOnClickListener(null);
                            var3.setText("");
                            game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).setVarThree("");
                            sharedPreferences.edit().putBoolean("isX2usedInLastRound",true).apply();
                        }

                    }
                }
            });

            var4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (game.getIsX2Used() == 0 || sharedPreferences.contains("isX2usedInLastRound")) {
                        circularViewWithTimer.stopTimer();
                        if (((game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarFour()).equals(game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer()))) {
                            questionsInRowWithoutHints = questionsInRowWithoutHints + 1;
                            if(questionsInRowWithoutHints > user.getLongestQuestionSeriesWithoutHintsWithinOneGame()) {
                                myDB.updateLongestQuestionSeriesWithoutHintsWithinOneGame(user.getUserName(), questionsInRowWithoutHints);
                                user.setLongestQuestionSeriesWithoutHintsWithinOneGame(questionsInRowWithoutHints);
                            }
                            if(game.getCurrentQuestionNumber() == 13){
                                game.setCurrentMoneyAmountEarned(getCurrentQuestionPrize(game.getCurrentQuestionNumber()));
                                takeCash.setVisibility(View.INVISIBLE);
                                takeCash.setOnClickListener(null);
                                dialogYouWon();
                            }else{
                                game.setCurrentMoneyAmountEarned(getCurrentQuestionPrize(game.getCurrentQuestionNumber()));
                                game.setCurrentQuestionNumber(game.getCurrentQuestionNumber() + 1);
                                startGame(game);
                            }
                        } else {
                            dialogYouLost(4);
                        }
                    } else if (game.getIsX2Used() == 1){
                        if (((game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarFour()).equals(game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer()))) {
                            if(questionsInRowWithoutHints > user.getLongestQuestionSeriesWithoutHintsWithinOneGame()) {
                                myDB.updateLongestQuestionSeriesWithoutHintsWithinOneGame(user.getUserName(), questionsInRowWithoutHints);
                                user.setLongestQuestionSeriesWithoutHintsWithinOneGame(questionsInRowWithoutHints);
                            }
                            questionsInRowWithoutHints = 0;
                            sharedPreferences.edit().putBoolean("isX2usedInLastRound",true).apply();
                            rhombusTv.setText("");
                            if(game.getCurrentQuestionNumber() == 13){
                                game.setCurrentMoneyAmountEarned(getCurrentQuestionPrize(game.getCurrentQuestionNumber()));
                                takeCash.setVisibility(View.INVISIBLE);
                                takeCash.setOnClickListener(null);
                                dialogYouWon();
                            }else {
                                sharedPreferences.edit().putBoolean("x2CurrentlyUsing", false).apply();
                                game.setCurrentMoneyAmountEarned(getCurrentQuestionPrize(game.getCurrentQuestionNumber()));
                                game.setCurrentQuestionNumber(game.getCurrentQuestionNumber() + 1);
                                startGame(game);
                            }
                        }else{
                            rhombusTv.setText("");
                            var4.setOnClickListener(null);
                            var4.setText("");
                            game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).setVarFour("");
                            sharedPreferences.edit().putBoolean("isX2usedInLastRound",true).apply();
                        }

                    }
                }
            });
        }
    }

    private int[] giveResults(int givenProbability,String[] questionVars, int correctAnswerPosition){
       int resultCorrect, resultOne, resultTwo, resultThree, resultFour, low, high;

        Random r = new Random();
        if(givenProbability >= 20) {
            low = 45;
            high = 60;
            resultCorrect = r.nextInt(high - low) + low;
        }else{
            low = 20;
            high = 35;
            resultCorrect = r.nextInt(high - low) + low;
        }
        int[] prob = {0,0,0,0};

        int emptyCount = 0;
        int nonEmptyIndex = 0;
        for(int i = 0; i<questionVars.length;i++){
            if(questionVars[i].equals("")){
                emptyCount++;
            }else{
                nonEmptyIndex = i;
            }
        }

        if(emptyCount == 3){
            switch (nonEmptyIndex){
                case 0:
                    prob[0] = 100;
                    return prob;
                case 1:
                    prob[1] = 100;
                    return prob;

                case 2:
                    prob[2] = 100;
                    return prob;
                case 3:
                    prob[3] = 100;
                    return prob;
            }
        }else {
            //1

            if (correctAnswerPosition == 0) {
                prob[0] = resultCorrect;

                if (!questionVars[1].equals("")) {
                    low = 20;
                    high = 100 - resultCorrect;
                    if (high < low) {
                        prob[1] = 0;
                    } else {
                        resultTwo = r.nextInt(high - low) + low;
                        if(resultTwo <0) resultTwo = r.nextInt(high - low) + low;
                        prob[1] = resultTwo;
                    }
                } else {
                    prob[1] = 0;
                }

                if (!questionVars[2].equals("") && prob[1] != 0) {
                    low = 5;
                    high = 100 - resultCorrect - prob[1];
                    if (high < low) {
                        prob[2] = 0;

                    } else {
                        resultThree = r.nextInt(high - low) + low;
                        if(resultThree <0) resultThree = r.nextInt(high - low) + low;
                        prob[2] = resultThree;
                    }
                } else if (!questionVars[2].equals("") && prob[1] == 0) {
                    low = 20;
                    high = 100 - resultCorrect;
                    if (high > low) {
                        resultThree = r.nextInt(high - low) + low;
                        if(resultThree <0) resultThree = r.nextInt(high - low) + low;
                        prob[2] = resultThree;
                    } else {
                        prob[2] = 0;
                    }
                } else {
                    prob[2] = 0;
                }


                //here

                if (!questionVars[3].equals("") && prob[1] != 0 && prob[2] != 0) {
                    low = 1;
                    high = 100 - resultCorrect - prob[1] - prob[2];
                    if (high > low) {
                        resultFour = r.nextInt(high - low) + low;
                        if(resultFour <0) resultFour = r.nextInt(high - low) + low;
                        prob[3] = resultFour;
                    } else {
                        prob[3] = 0;
                    }
                } else if (!questionVars[3].equals("") && prob[1] == 0 && prob[2] != 0) {
                    low = 5;
                    high = 100 - resultCorrect - prob[2];
                    if (high > low) {
                        resultFour = r.nextInt(high - low) + low;
                        if(resultFour <0) resultFour = r.nextInt(high - low) + low;
                        prob[3] = resultFour;
                    } else {
                        prob[3] = 0;
                    }

                } else if (!questionVars[3].equals("") && prob[1] != 0 && prob[2] == 0) {
                    low = 5;
                    high = 100 - resultCorrect - prob[1];
                    if (high > low) {
                        resultFour = r.nextInt(high - low) + low;
                        if(resultFour <0) resultFour = r.nextInt(high - low) + low;
                        prob[3] = resultFour;
                    } else {
                        prob[3] = 0;
                    }
                } else if (!questionVars[3].equals("") && prob[1] == 0 && prob[2] == 0) {
                    prob[3] = 100 - resultCorrect;
                } else {
                    prob[3] = 0;
                }

            }

            //2

            if (correctAnswerPosition == 1) {
                prob[1] = resultCorrect;

                if (!questionVars[3].equals("")) {
                    low = 20;
                    high = 100 - resultCorrect;
                    if (high > low) {
                        resultFour = r.nextInt(high - low) + low;
                        if(resultFour <0) resultFour = r.nextInt(high - low) + low;
                        prob[3] = resultFour;
                    } else {
                        prob[3] = 0;
                    }
                } else {
                    prob[3] = 0;
                }

                if (!questionVars[2].equals("") && prob[3] != 0) {
                    low = 5;
                    high = 100 - resultCorrect - prob[3];
                    if (high > low) {
                        resultThree = r.nextInt(high - low) + low;
                        if(resultThree <0) resultThree = r.nextInt(high - low) + low;
                        prob[2] = resultThree;
                    } else {
                        prob[2] = 0;
                    }
                } else if (!questionVars[2].equals("") && prob[3] == 0) {
                    low = 20;
                    high = 100 - resultCorrect;
                    if (high > low) {
                        resultThree = r.nextInt(high - low) + low;
                        if(resultThree <0) resultThree = r.nextInt(high - low) + low;
                        prob[2] = resultThree;
                    } else {
                        prob[2] = 0;
                    }
                } else {
                    prob[2] = 0;
                }


                //here

                if (!questionVars[0].equals("") && prob[2] != 0 && prob[3] != 0) {
                    low = 1;
                    high = 100 - resultCorrect - prob[2] - prob[3];
                    if (high > low) {
                        resultOne = r.nextInt(high - low) + low;
                        if(resultOne <0) resultOne = r.nextInt(high - low) + low;
                        prob[0] = resultOne;
                    } else {
                        prob[0] = 0;
                    }
                } else if (!questionVars[0].equals("") && prob[2] == 0 && prob[3] != 0) {
                    low = 5;
                    high = 100 - resultCorrect - prob[3];
                    if (high > low) {
                        resultOne = r.nextInt(high - low) + low;
                        if(resultOne <0) resultOne = r.nextInt(high - low) + low;
                        prob[0] = resultOne;
                    } else {
                        prob[0] = 0;
                    }

                } else if (!questionVars[0].equals("") && prob[2] != 0 && prob[3] == 0) {
                    low = 5;
                    high = 100 - resultCorrect - prob[2];
                    if (high > low) {
                        resultOne = r.nextInt(high - low) + low;
                        if(resultOne <0) resultOne = r.nextInt(high - low) + low;
                        prob[0] = resultOne;
                    } else {
                        prob[0] = 0;
                    }
                } else if (!questionVars[0].equals("") && prob[2] == 0 && prob[3] == 0) {
                    prob[0] = 100 - resultCorrect;
                } else {
                    prob[0] = 0;
                }

            }

            //3

            if (correctAnswerPosition == 2) {
                prob[2] = resultCorrect;

                if (!questionVars[1].equals("")) {
                    low = 20;
                    high = 100 - resultCorrect;
                    if (high < low) {
                        prob[1] = 0;
                    } else {
                        resultTwo = r.nextInt(high - low) + low;
                        if(resultTwo <0) resultTwo = r.nextInt(high - low) + low;
                        prob[1] = resultTwo;
                    }
                } else {
                    prob[1] = 0;
                }


                if (!questionVars[0].equals("") && prob[1] != 0) {
                    low = 5;
                    high = 100 - resultCorrect - prob[1];
                    if (high < low) {
                        prob[0] = 0;

                    } else {
                        resultOne = r.nextInt(high - low) + low;
                        if(resultOne <0) resultOne = r.nextInt(high - low) + low;
                        prob[0] = resultOne;
                    }
                } else if (!questionVars[0].equals("") && prob[1] == 0) {
                    low = 20;
                    high = 100 - resultCorrect;
                    if (high > low) {
                        resultOne = r.nextInt(high - low) + low;
                        if(resultOne <0) resultOne = r.nextInt(high - low) + low;
                        prob[0] = resultOne;
                    } else {
                        prob[0] = 0;
                    }
                } else {
                    prob[0] = 0;
                }


                //here


                if (!questionVars[3].equals("") && prob[1] != 0 && prob[0] != 0) {
                    low = 1;
                    high = 100 - resultCorrect - prob[1] - prob[0];
                    if (high > low) {
                        resultFour = r.nextInt(high - low) + low;
                        if(resultFour<0)resultFour = r.nextInt(high - low) + low;
                        prob[3] = resultFour;
                    } else {
                        prob[3] = 0;
                    }
                } else if (!questionVars[3].equals("") && prob[1] == 0 && prob[0] != 0) {
                    low = 5;
                    high = 100 - resultCorrect - prob[0];
                    if (high > low) {
                        resultFour = r.nextInt(high - low) + low;
                        if(resultFour<0)resultFour = r.nextInt(high - low) + low;
                        prob[3] = resultFour;
                    } else {
                        prob[3] = 0;
                    }

                } else if (!questionVars[3].equals("") && prob[1] != 0 && prob[0] == 0) {
                    low = 5;
                    high = 100 - resultCorrect - prob[1];
                    if (high > low) {
                        resultFour = r.nextInt(high - low) + low;
                        if(resultFour<0)resultFour = r.nextInt(high - low) + low;
                        prob[3] = resultFour;
                    } else {
                        prob[3] = 0;
                    }
                } else if (!questionVars[3].equals("") && prob[1] == 0 && prob[0] == 0) {
                    prob[3] = 100 - resultCorrect;
                } else {
                    prob[3] = 0;
                }

            }


            //4


            if (correctAnswerPosition == 3) {
                prob[3] = resultCorrect;

                if (!questionVars[2].equals("")) {
                    low = 20;
                    high = 100 - resultCorrect;
                    if (high > low) {
                        resultThree = r.nextInt(high - low) + low;
                        if(resultThree<0)resultThree = r.nextInt(high - low) + low;
                        prob[2] = resultThree;
                    } else {
                        prob[2] = 0;
                    }
                } else {
                    prob[2] = 0;
                }

                if (!questionVars[1].equals("") && prob[2] != 0) {
                    low = 5;
                    high = 100 - resultCorrect - prob[2];
                    if (high > low) {
                        resultTwo = r.nextInt(high - low) + low;
                        if(resultTwo<0)resultTwo = r.nextInt(high - low) + low;
                        prob[1] = resultTwo;
                    } else {
                        prob[1] = 0;
                    }
                } else if (!questionVars[1].equals("") && prob[2] == 0) {
                    low = 20;
                    high = 100 - resultCorrect;
                    if (high > low) {
                        resultTwo = r.nextInt(high - low) + low;
                        if(resultTwo<0)resultTwo = r.nextInt(high - low) + low;
                        prob[1] = resultTwo;
                    } else {
                        prob[1] = 0;
                    }
                } else {
                    prob[1] = 0;
                }

                //here

                if (!questionVars[0].equals("") && prob[1] != 0 && prob[2] != 0) {
                    low = 1;
                    high = 100 - resultCorrect - prob[1] - prob[2];
                    if (high > low) {
                        resultOne = r.nextInt(high - low) + low;
                        if(resultOne<0)resultOne = r.nextInt(high - low) + low;
                        prob[0] = resultOne;
                    } else {
                        prob[0] = 0;
                    }
                } else if (!questionVars[0].equals("") && prob[1] == 0 && prob[2] != 0) {
                    low = 5;
                    high = 100 - resultCorrect - prob[2];
                    if (high > low) {
                        resultOne = r.nextInt(high - low) + low;
                        if(resultOne<0)resultOne = r.nextInt(high - low) + low;
                        prob[0] = resultOne;
                    } else {
                        prob[0] = 0;
                    }

                } else if (!questionVars[0].equals("") && prob[1] != 0 && prob[2] == 0) {
                    low = 5;
                    high = 100 - resultCorrect - prob[1];
                    if (high > low) {
                        resultOne = r.nextInt(high - low) + low;
                        if(resultOne<0)resultOne = r.nextInt(high - low) + low;
                        prob[0] = resultOne;
                    } else {
                        prob[0] = 0;
                    }
                } else if (!questionVars[0].equals("") && prob[1] == 0 && prob[2] == 0) {
                    prob[0] = 100 - resultCorrect;
                } else {
                    prob[0] = 0;
                }

            }

            return prob;
        }

        return prob;
    }

    private void launchTimer() {
        CircularView.OptionsBuilder builderWithTimer =
                new CircularView.OptionsBuilder()
                        .shouldDisplayText(true)
                        .setCounterInSeconds(58)
                        .setCircularViewCallback(new CircularViewCallback() {
                            @Override
                            public void onTimerFinish() {
                                sharedPreferences.edit().putBoolean("isTimerRunning", false).apply();
                                dialogOutOfTime();
                            }

                            @Override
                            public void onTimerCancelled() {

                            }
                        });

        circularViewWithTimer.setOptions(builderWithTimer);

        circularViewWithTimer.startTimer();

        sharedPreferences.edit().putBoolean("isTimerRunning", true).apply();

    }


    private Game initGame() {
        int gameId = getGameID();
        String stableSummString = sharedPreferences.getString("stableSumm", "").replaceAll("\\s+", "");
        int stableSumm = Integer.valueOf(stableSummString);
        Map<Integer, Question> questionsInCurrentGame = getQuestionsInGame();
        game = new Game(user, gameId, 0, 0, 0, 0, 1, 0, stableSumm, questionsInCurrentGame);

        return game;
    }




    private void dialogAudienceHelp(String[] questionVars, int[] prob){
        circularViewWithTimer.pauseTimer();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(false);
        final View mView = getLayoutInflater().inflate(R.layout.dialog_audience_help, null);
        final TextView helpTV = mView.findViewById(R.id.audience_help);
        final TextView var1 = mView.findViewById(R.id.tv_var_1);
        final TextView var2 = mView.findViewById(R.id.tv_var_2);
        final TextView var3 = mView.findViewById(R.id.tv_var_3);
        final TextView var4 = mView.findViewById(R.id.tv_var_4);
        final TextView trust = mView.findViewById(R.id.trust_or_not);
        helpTV.setText(user.getUserName() + ", зрители считают, что правильный ответ:");
        if(!questionVars[0].equals("")){
            var1.setVisibility(View.VISIBLE);
            var1.setText("A: " + questionVars[0] + " -  с вероятностью " + prob[0] + "% ");
        }
        if(!questionVars[1].equals("")){
            var2.setVisibility(View.VISIBLE);
            var2.setText("B: " + questionVars[1] + " - с вероятностью " + prob[1] + "% ");
        }
        if(!questionVars[2].equals("")){
            var3.setVisibility(View.VISIBLE);
            var3.setText("C: " + questionVars[2] + " - с вероятностью " + prob[2] + "% ");
        }
        if(!questionVars[3].equals("")){
            var4.setVisibility(View.VISIBLE);
            var4.setText("D: " + questionVars[3] + " - с вероятностью " + prob[3] + "% ");
        }
        trust.setText("А верить зрителям или нет, решай сам :)");


        alertDialog.setView(mView);
        alertDialog.setPositiveButton("Продолжить играть", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                circularViewWithTimer.resumeTimer();
            }

        });
        AlertDialog dialog = alertDialog.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void dialogYouWon() {
        circularViewWithTimer.stopTimer();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(false);
        final View mView = getLayoutInflater().inflate(R.layout.dialog_you_won, null);
        TextView youWonTv  = mView.findViewById(R.id.you_won_text);
        youWonTv.setText(user.getUserName() + ", поздравляем тебя с победой!\nЭто невероятный успех! Хочешь поделиться ним с друзьями?");
        alertDialog.setView(mView);
        alertDialog.setPositiveButton("Разумеется!", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                shareIntentWon();
                dialog.dismiss();

            }

        });
        alertDialog.setNegativeButton("Нет, не хочу", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = alertDialog.create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                int amountOfHintsUsed = 0;
                myDB = new DatabaseHelper(getContext());
                myDB.updateGamesCompleted(user.getUserName(), user.getGamesCompleted() + 1);
                myDB.updateWalletBalance(user.getUserName(), user.getWalletBalance() + game.getCurrentMoneyAmountEarned());
                myDB.updateMaxRewardCount(user.getUserName(),user.getMaxRewardCount()+1);
                if((game.getIsX2Used() == 0) && game.getIs5050Used()  == 0 && game.getIsAudienceHintUsed() == 0 && game.getIsCallToFriendUsed() == 0){
                    myDB.updateGamesCompletedWithoutHints(user.getUserName(), user.getGamesCompletedWithoutHints() + 1);
                }
                myDB.updatePoints(user.getUserName(),user.getPoints() + getPointsRatio(game.getCurrentQuestionNumber()));
                if(game.getIsX2Used() ==1){
                    myDB.updateAmountX2Used(user.getUserName(), user.getAmountX2Used() + 1);
                    amountOfHintsUsed = amountOfHintsUsed + 1;
                }
                if(game.getIs5050Used() ==1){
                    myDB.updateAmountFiftyFiftyUsed(user.getUserName(), user.getAmountFiftyFiftyUsed() + 1);
                    amountOfHintsUsed = amountOfHintsUsed + 1;
                }
                if(game.getIsAudienceHintUsed() ==1){
                    myDB.updateAmountAudienceUsed(user.getUserName(), user.getAmountAudienceUsed() + 1);
                    amountOfHintsUsed = amountOfHintsUsed + 1;
                }
                if(game.getIsCallToFriendUsed() ==1){
                    myDB.updateAmountCallUsed(user.getUserName(), user.getAmountCallUsed() + 1);
                    amountOfHintsUsed = amountOfHintsUsed + 1;

                }
                if(questionsInRowWithoutHints >= user.getLongestQuestionSeriesWithoutHintsWithinOneGame()){
                    myDB.updateLongestQuestionSeriesWithoutHintsWithinOneGame(user.getUserName(), questionsInRowWithoutHints);
                }


                Locale locale = new Locale("ru");
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM HH:mm", locale);
                formattedDate = sdf.format(new Date());
                myDB.insertToGameHistory(user.getUserName(), game.getGameID(), formattedDate, game.getCurrentMoneyAmountEarned(),  amountOfHintsUsed, game.getStableSumm(), game.getIsX2Used(), game.getIs5050Used(), game.getIsCallToFriendUsed(), game.getIsAudienceHintUsed(), game.getCurrentQuestionNumber(), game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionText(), 1,game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer(), game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer());
                myDB.close();
                getBackWithResult();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    private void dialogProgressLost() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(false);
        final View mView = getLayoutInflater().inflate(R.layout.dialog_progress_lost, null);
        TextView progLost  = mView.findViewById(R.id.prog_lost);
        progLost.setText("Если выйдешь сейчас, то текущий прогресс не будет сохранен.\nТы действительно хочешь выйти?");
        alertDialog.setView(mView);
        alertDialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                circularViewWithTimer.stopTimer();
                FragmentManager fragmentManager;
                FragmentTransaction fragmentTransaction;
                fragmentManager = getFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction
                        .replace(R.id.fragment_container, new MenuFragment())
                        .commit();
                dialog.dismiss();
            }
        });
        alertDialog.setNegativeButton("Вернуться в игру", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = alertDialog.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void dialogFriendHelp(String answer){
        circularViewWithTimer.pauseTimer();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(false);
        final View mView = getLayoutInflater().inflate(R.layout.dialog_friend_help, null);
        final TextView helpTV = mView.findViewById(R.id.friend_help);
        String cap = answer.substring(0, 1).toUpperCase() + answer.substring(1);
        helpTV.setText(user.getUserName() + ", твой друг считает, что правильный ответ: \n" + cap + ".\nА верить ему или нет, ты уж решай сам.");
        alertDialog.setView(mView);
        alertDialog.setPositiveButton("Продолжить играть", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                circularViewWithTimer.resumeTimer();
                dialog.dismiss();
            }

        });
        AlertDialog dialog = alertDialog.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    private void dialogGetMoney() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(false);
        final View mView = getLayoutInflater().inflate(R.layout.dialog_get_money, null);
        final TextView moneyTV = mView.findViewById(R.id.text_money_amount);
        moneyTV.setText(user.getUserName() + ", хочешь забрать " + game.getCurrentMoneyAmountEarned()+ " Р. ?");
        alertDialog.setView(mView);
        alertDialog.setPositiveButton("Забрать деньги", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogShareWithFriends();
                dialog.dismiss();
            }

        });
        alertDialog.setNegativeButton("Продолжить играть", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = alertDialog.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void dialogShareWithFriends() {
        circularViewWithTimer.stopTimer();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(false);
        final View mView = getLayoutInflater().inflate(R.layout.dialog_send_result_to_friends, null);
        alertDialog.setView(mView);
        alertDialog.setPositiveButton("Да, очень интересно!", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                shareIntent();
                dialog.dismiss();

            }

        });
        alertDialog.setNegativeButton("Нет, не хочу", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                 dialog.dismiss();
            }
        });
        AlertDialog dialog = alertDialog.create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                int amountOfHintsUsed = 0;
                myDB = new DatabaseHelper(getContext());
                myDB.updateGamesCompleted(user.getUserName(), user.getGamesCompleted() + 1);
                myDB.updateWalletBalance(user.getUserName(), user.getWalletBalance() + game.getCurrentMoneyAmountEarned());
                if((game.getIsX2Used() == 0) && game.getIs5050Used()  == 0 && game.getIsAudienceHintUsed() == 0 && game.getIsCallToFriendUsed() == 0){
                    myDB.updateGamesCompletedWithoutHints(user.getUserName(), user.getGamesCompletedWithoutHints() + 1);
                }
                myDB.updatePoints(user.getUserName(),user.getPoints() + getPointsRatio(game.getCurrentQuestionNumber()));
                if(game.getIsX2Used() ==1){
                    myDB.updateAmountX2Used(user.getUserName(), user.getAmountX2Used() + 1);
                    amountOfHintsUsed=amountOfHintsUsed+1;
                }
                if(game.getIs5050Used() ==1){
                    myDB.updateAmountFiftyFiftyUsed(user.getUserName(), user.getAmountFiftyFiftyUsed() + 1);
                    amountOfHintsUsed=amountOfHintsUsed+1;
                }
                if(game.getIsAudienceHintUsed() ==1){
                    myDB.updateAmountAudienceUsed(user.getUserName(), user.getAmountAudienceUsed() + 1);
                    amountOfHintsUsed=amountOfHintsUsed+1;
                }
                if(game.getIsCallToFriendUsed() ==1){
                    myDB.updateAmountCallUsed(user.getUserName(), user.getAmountCallUsed() + 1);
                    amountOfHintsUsed=amountOfHintsUsed+1;
                }
                if(questionsInRowWithoutHints >= user.getLongestQuestionSeriesWithoutHintsWithinOneGame()){
                    myDB.updateLongestQuestionSeriesWithoutHintsWithinOneGame(user.getUserName(), questionsInRowWithoutHints);
                }

                Locale locale = new Locale("ru");
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM HH:mm", locale);
                formattedDate = sdf.format(new Date());
                myDB.insertToGameHistory(user.getUserName(), game.getGameID(), formattedDate, game.getCurrentMoneyAmountEarned(),  amountOfHintsUsed, game.getStableSumm(), game.getIsX2Used(), game.getIs5050Used(), game.getIsCallToFriendUsed(), game.getIsAudienceHintUsed(), game.getCurrentQuestionNumber(), game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionText(), 0,"", game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer());
                myDB.close();
                getBackWithResult();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    private void dialogYouLost(final int variant) {
        circularViewWithTimer.stopTimer();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(false);
        final View mView = getLayoutInflater().inflate(R.layout.dialog_you_lost, null);
        alertDialog.setView(mView);
        alertDialog.setPositiveButton("Да, хочу спросить друзей", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (variant){
                    case 1:
                        shareIntentLost(1);
                        break;
                    case 2:
                        shareIntentLost(2);
                        break;
                    case 3:
                        shareIntentLost(3);
                        break;
                    case 4:
                        shareIntentLost(4);
                        break;
                }

                dialog.dismiss();
            }

        });
        alertDialog.setNegativeButton("Нет, не хочу", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = alertDialog.create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if(doesStableSummHaveBeenReached()){
                    int amountOfHintsUsed=0;
                    String userAnswer = "";
                    myDB = new DatabaseHelper(getContext());
                    myDB.updateGamesCompleted(user.getUserName(), user.getGamesCompleted() + 1);
                    myDB.updateWalletBalance(user.getUserName(), user.getWalletBalance() + game.getStableSumm());
                    if((game.getIsX2Used() == 0) && game.getIs5050Used()  == 0 && game.getIsAudienceHintUsed() == 0 && game.getIsCallToFriendUsed() == 0){
                        myDB.updateGamesCompletedWithoutHints(user.getUserName(), user.getGamesCompletedWithoutHints() + 1);
                    }
                    myDB.updatePoints(user.getUserName(),user.getPoints() + getPointsRatio(game.getCurrentQuestionNumber()));
                    if(game.getIsX2Used() ==1){
                        myDB.updateAmountX2Used(user.getUserName(), user.getAmountX2Used() + 1);
                        amountOfHintsUsed=amountOfHintsUsed+1;
                    }
                    if(game.getIs5050Used() ==1){
                        myDB.updateAmountFiftyFiftyUsed(user.getUserName(), user.getAmountFiftyFiftyUsed() + 1);
                        amountOfHintsUsed=amountOfHintsUsed+1;
                    }
                    if(game.getIsAudienceHintUsed() ==1){
                        myDB.updateAmountAudienceUsed(user.getUserName(), user.getAmountAudienceUsed() + 1);
                        amountOfHintsUsed=amountOfHintsUsed+1;
                    }
                    if(game.getIsCallToFriendUsed() ==1){
                        myDB.updateAmountCallUsed(user.getUserName(), user.getAmountCallUsed() + 1);
                        amountOfHintsUsed=amountOfHintsUsed+1;
                    }
                    if(questionsInRowWithoutHints >= user.getLongestQuestionSeriesWithoutHintsWithinOneGame()){
                        myDB.updateLongestQuestionSeriesWithoutHintsWithinOneGame(user.getUserName(), questionsInRowWithoutHints);
                    }

                    switch (variant){
                        case 1:
                            userAnswer = game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarOne();
                            break;
                        case 2:
                            userAnswer = game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarTwo();
                            break;
                        case 3:
                            userAnswer = game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarThree();
                            break;
                        case 4:
                            userAnswer = game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarFour();
                            break;
                    }

                    Locale locale = new Locale("ru");
                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM HH:mm", locale);
                    formattedDate = sdf.format(new Date());
                    myDB.insertToGameHistory(user.getUserName(), game.getGameID(), formattedDate, game.getStableSumm(),  amountOfHintsUsed, game.getStableSumm(), game.getIsX2Used(), game.getIs5050Used(), game.getIsCallToFriendUsed(), game.getIsAudienceHintUsed(), game.getCurrentQuestionNumber(), game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionText(), 1,userAnswer, game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer());

                    myDB.close();
                    getBackWithResult();
                }else{
                    int amountOfHintsUsed=0;
                    String userAnswer = "";
                    myDB = new DatabaseHelper(getContext());
                    myDB.updateGamesCompleted(user.getUserName(), user.getGamesCompleted() + 1);
                    if((game.getIsX2Used() == 0) && game.getIs5050Used()  == 0 && game.getIsAudienceHintUsed() == 0 && game.getIsCallToFriendUsed() == 0){
                        myDB.updateGamesCompletedWithoutHints(user.getUserName(), user.getGamesCompletedWithoutHints() + 1);
                    }
                    myDB.updatePoints(user.getUserName(),user.getPoints() + getPointsRatio(game.getCurrentQuestionNumber()));
                    if(game.getIsX2Used() ==1){
                        myDB.updateAmountX2Used(user.getUserName(), user.getAmountX2Used() + 1);
                        amountOfHintsUsed=amountOfHintsUsed+1;
                    }
                    if(game.getIs5050Used() ==1){
                        myDB.updateAmountFiftyFiftyUsed(user.getUserName(), user.getAmountFiftyFiftyUsed() + 1);
                        amountOfHintsUsed=amountOfHintsUsed+1;
                    }
                    if(game.getIsAudienceHintUsed() ==1){
                        myDB.updateAmountAudienceUsed(user.getUserName(), user.getAmountAudienceUsed() + 1);
                        amountOfHintsUsed=amountOfHintsUsed+1;
                    }
                    if(game.getIsCallToFriendUsed() ==1){
                        myDB.updateAmountCallUsed(user.getUserName(), user.getAmountCallUsed() + 1);
                        amountOfHintsUsed=amountOfHintsUsed+1;
                    }
                    if(questionsInRowWithoutHints >= user.getLongestQuestionSeriesWithoutHintsWithinOneGame()){
                        myDB.updateLongestQuestionSeriesWithoutHintsWithinOneGame(user.getUserName(), questionsInRowWithoutHints);
                    }

                    switch (variant){
                        case 1:
                            userAnswer = game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarOne();
                            break;
                        case 2:
                            userAnswer = game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarTwo();
                            break;
                        case 3:
                            userAnswer = game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarThree();
                            break;
                        case 4:
                            userAnswer = game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getVarFour();
                            break;
                    }

                    Locale locale = new Locale("ru");
                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM HH:mm", locale);
                    formattedDate = sdf.format(new Date());
                    myDB.insertToGameHistory(user.getUserName(), game.getGameID(), formattedDate, 0,  amountOfHintsUsed, game.getStableSumm(), game.getIsX2Used(), game.getIs5050Used(), game.getIsCallToFriendUsed(), game.getIsAudienceHintUsed(), game.getCurrentQuestionNumber(), game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionText(), 1,userAnswer, game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer());

                    myDB.close();
                    getBackWithResult();
                }
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void shareIntentWon(){
        Intent mSharingIntent = new Intent(Intent.ACTION_SEND);
        mSharingIntent.setType("text/plain");
        String textSharing = "Я играл в игру \"Кто хочет стать миллионером?\",и выиграл 3 000 000 рублей!\n";
        textSharing = textSharing + "А ты бы ответил на последний вопрос? Текст вопроса и варианты ответа приведены ниже.";
        textSharing = textSharing + (game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionText()) + "\n";
        textSharing = textSharing + "Были вот такие варианты ответа: \n" + "A: " + sharedPreferences.getString("currentVarOne","") + "\nB: " + sharedPreferences.getString("currentVarTwo","") + "\nC: " + sharedPreferences.getString("currentVarThree","") + "\nD: " + sharedPreferences.getString("currentVarFour","") + "\n";
        textSharing = textSharing + "Если у тебя еще нет этой классной игрушки, то ты можешь скачать ее здесь: link";
        mSharingIntent.putExtra(Intent.EXTRA_TEXT, textSharing);
        startActivity(Intent.createChooser(mSharingIntent, "Отправить вопрос другу"));
    }

    private void shareIntentLost(int variant){
        Intent mSharingIntent = new Intent(Intent.ACTION_SEND);
        mSharingIntent.setType("text/plain");
        String textSharing = "Я играл в игру \"Кто хочет стать миллионером?\", но дал неправильный ответ на этот вопрос: \n";
        textSharing = textSharing + (game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionText()) + "\n";
        textSharing = textSharing + "Были вот такие варианты ответа: \n" + "A: " + sharedPreferences.getString("currentVarOne","") + "\nB: " + sharedPreferences.getString("currentVarTwo","") + "\nC: " + sharedPreferences.getString("currentVarThree","") + "\nD: " + sharedPreferences.getString("currentVarFour","") + "\n";
        switch (variant){
            case 1:
                textSharing = textSharing + "Может быть ты знаешь правильный ответ на этот вопрос? Я ответил A: " + sharedPreferences.getString("currentVarOne","") + "\n";
                break;
            case 2:
                textSharing = textSharing + "Может быть ты знаешь правильный ответ на этот вопрос? Я ответил B: " + sharedPreferences.getString("currentVarTwo","") + "\n";
                break;
            case 3:
                textSharing = textSharing + "Может быть ты знаешь правильный ответ на этот вопрос? Я ответил C: " + sharedPreferences.getString("currentVarThree","") + "\n";
                break;
            case 4:
                textSharing = textSharing + "Может быть ты знаешь правильный ответ на этот вопрос? Я ответил D: " + sharedPreferences.getString("currentVarFour","") + "\n";
                break;
        }
        textSharing = textSharing + "А какой вариант из оставшихся трех является правильным на твой взгляд?\n";
        textSharing = textSharing + "Если у тебя еще нет этой классной игрушки, то ты можешь скачать ее здесь: link";
        mSharingIntent.putExtra(Intent.EXTRA_TEXT, textSharing);
        startActivity(Intent.createChooser(mSharingIntent, "Отправить вопрос другу"));
    }

    private void shareIntent(){
        Intent mSharingIntent = new Intent(Intent.ACTION_SEND);
        mSharingIntent.setType("text/plain");
        String textSharing = "Я играл в игру \"Кто хочет стать миллионером?\", и мне попался вот такой вопрос: \n";
        textSharing = textSharing + (game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionText()) + "\n";
        textSharing = textSharing + "Были вот такие варианты ответа: \n" + "A: " + sharedPreferences.getString("currentVarOne","") + "\nB: " + sharedPreferences.getString("currentVarTwo","") + "\nC: " + sharedPreferences.getString("currentVarThree","") + "\nD: " + sharedPreferences.getString("currentVarFour","") + "\n";
        textSharing = textSharing + "Может быть ты знаешь правильный ответ на этот вопрос? Мне пришлось забрать " + game.getCurrentMoneyAmountEarned() + " Р. \n";
        textSharing = textSharing + "Если у тебя еще нет этой классной игрушки, то ты можешь скачать ее здесь: link";

        mSharingIntent.putExtra(Intent.EXTRA_TEXT, textSharing);
        startActivity(Intent.createChooser(mSharingIntent, "Отправить вопрос другу"));
    }

    private void dialogOutOfTime() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(false);
        final View mView = getLayoutInflater().inflate(R.layout.dialog_out_of_time, null);
        alertDialog.setView(mView);
        alertDialog.setPositiveButton("Понятно....", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(doesStableSummHaveBeenReached()){
                    int amountOfHintsUsed = 0;
                    myDB = new DatabaseHelper(getContext());
                    myDB.updateGamesCompleted(user.getUserName(), user.getGamesCompleted() + 1);
                    myDB.updateWalletBalance(user.getUserName(), user.getWalletBalance() + game.getStableSumm());
                    if((game.getIsX2Used() == 0) && game.getIs5050Used()  == 0 && game.getIsAudienceHintUsed() == 0 && game.getIsCallToFriendUsed() == 0){
                        myDB.updateGamesCompletedWithoutHints(user.getUserName(), user.getGamesCompletedWithoutHints() + 1);
                    }
                    myDB.updatePoints(user.getUserName(),user.getPoints() + getPointsRatio(game.getCurrentQuestionNumber()));
                    if(game.getIsX2Used() ==1){
                        myDB.updateAmountX2Used(user.getUserName(), user.getAmountX2Used() + 1);
                        amountOfHintsUsed=amountOfHintsUsed+1;
                    }
                    if(game.getIs5050Used() ==1){
                        myDB.updateAmountFiftyFiftyUsed(user.getUserName(), user.getAmountFiftyFiftyUsed() + 1);
                        amountOfHintsUsed=amountOfHintsUsed+1;
                    }
                    if(game.getIsAudienceHintUsed() ==1){
                        myDB.updateAmountAudienceUsed(user.getUserName(), user.getAmountAudienceUsed() + 1);
                        amountOfHintsUsed=amountOfHintsUsed+1;
                    }
                    if(game.getIsCallToFriendUsed() ==1){
                        myDB.updateAmountCallUsed(user.getUserName(), user.getAmountCallUsed() + 1);
                        amountOfHintsUsed=amountOfHintsUsed+1;
                    }
                    if(questionsInRowWithoutHints >= user.getLongestQuestionSeriesWithoutHintsWithinOneGame()){
                        myDB.updateLongestQuestionSeriesWithoutHintsWithinOneGame(user.getUserName(), questionsInRowWithoutHints);
                    }


                    Locale locale = new Locale("ru");
                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM HH:mm", locale);
                    formattedDate = sdf.format(new Date());
                    myDB.insertToGameHistory(user.getUserName(), game.getGameID(), formattedDate, game.getStableSumm(),  amountOfHintsUsed, game.getStableSumm(), game.getIsX2Used(), game.getIs5050Used(), game.getIsCallToFriendUsed(), game.getIsAudienceHintUsed(), game.getCurrentQuestionNumber(), game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionText(), 0,"", game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer());

                    myDB.close();
                    getBackWithResult();
                }else{
                    int amountOfHintsUsed = 0;
                    myDB = new DatabaseHelper(getContext());
                    myDB.updateGamesCompleted(user.getUserName(), user.getGamesCompleted() + 1);
                    if((game.getIsX2Used() == 0) && game.getIs5050Used()  == 0 && game.getIsAudienceHintUsed() == 0 && game.getIsCallToFriendUsed() == 0){
                        myDB.updateGamesCompletedWithoutHints(user.getUserName(), user.getGamesCompletedWithoutHints() + 1);
                    }
                    myDB.updatePoints(user.getUserName(),user.getPoints() + getPointsRatio(game.getCurrentQuestionNumber()));
                    if(game.getIsX2Used() ==1){
                        myDB.updateAmountX2Used(user.getUserName(), user.getAmountX2Used() + 1);
                        amountOfHintsUsed=amountOfHintsUsed+1;
                    }
                    if(game.getIs5050Used() ==1){
                        myDB.updateAmountFiftyFiftyUsed(user.getUserName(), user.getAmountFiftyFiftyUsed() + 1);
                        amountOfHintsUsed=amountOfHintsUsed+1;
                    }
                    if(game.getIsAudienceHintUsed() ==1){
                        myDB.updateAmountAudienceUsed(user.getUserName(), user.getAmountAudienceUsed() + 1);
                        amountOfHintsUsed=amountOfHintsUsed+1;
                    }
                    if(game.getIsCallToFriendUsed() ==1){
                        myDB.updateAmountCallUsed(user.getUserName(), user.getAmountCallUsed() + 1);
                        amountOfHintsUsed=amountOfHintsUsed+1;
                    }
                    if(questionsInRowWithoutHints >= user.getLongestQuestionSeriesWithoutHintsWithinOneGame()){
                        myDB.updateLongestQuestionSeriesWithoutHintsWithinOneGame(user.getUserName(), questionsInRowWithoutHints);
                    }

                    Locale locale = new Locale("ru");
                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM HH:mm", locale);
                    formattedDate = sdf.format(new Date());
                    myDB.insertToGameHistory(user.getUserName(), game.getGameID(), formattedDate, 0,  amountOfHintsUsed, game.getStableSumm(), game.getIsX2Used(), game.getIs5050Used(), game.getIsCallToFriendUsed(), game.getIsAudienceHintUsed(), game.getCurrentQuestionNumber(), game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionText(), 0,"", game.getQuestionsInGame().get(game.getCurrentQuestionNumber()).getQuestionCorrectAnswer());

                    myDB.close();
                    getBackWithResult();
                }
                dialog.dismiss();
            }

        });

        AlertDialog dialog = alertDialog.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private int getGameID() {
        if (sharedPreferences.contains("lastGameId")) {
            sharedPreferences.edit().putInt("lastGameId",sharedPreferences.getInt("lastGameId",0)+1).apply();
            return sharedPreferences.getInt("lastGameId", 0);
        } else {
            sharedPreferences.edit().putInt("lastGameId", 1).apply();
            return sharedPreferences.getInt("lastGameId", 0);
        }
    }


    private int getPointsRatio(int currentQuestionNumber){
        switch (currentQuestionNumber){
            case 1:
                return 0;
            case 2:
                return 50;
            case 3:
                return 75;
            case 4:
                return 100;
            case 5:
                return 125;
            case 6:
                return 175;
            case 7:
                return 225;
            case 8:
                return 275;
            case 9:
                return 325;
            case 10:
                return 425;
            case 11:
                return 525;
            case 12:
                return 625;
            case 13:
                return 750;
        }
        return 0;
    }

    private int getCurrentQuestionPrize(int currentQuestionNumber) {
        switch (currentQuestionNumber) {
            case 1:
                return 500;
            case 2:
                return 1000;
            case 3:
                return 2000;
            case 4:
                return 5000;
            case 5:
                return 10000;
            case 6:
                return 25000;
            case 7:
                return 50000;
            case 8:
                return 100000;
            case 9:
                return 200000;
            case 10:
                return 400000;
            case 11:
                return 800000;
            case 12:
                return 1500000;
            case 13:
                return 3000000;
        }

        return 1;
    }

    private Map<Integer, Question> getQuestionsInGame() {
        Map<Integer, Question> map = new HashMap<>();
        for(int i=1;i<=13;i++) {
            map.put(i, getQuestion(i));
        }
        return map;
    }

    private void renderView(Game game) {
        int stableSumm = game.getStableSumm();
        int currentQuestion = game.getCurrentQuestionNumber();
        String currentQuestionText = (game.getQuestionsInGame().get(currentQuestion)).getQuestionText();
        String currentQuestionVarOne = (game.getQuestionsInGame().get(currentQuestion)).getVarOne();
        String currentQuestionVarTwo = (game.getQuestionsInGame().get(currentQuestion)).getVarTwo();
        String currentQuestionVarThree = (game.getQuestionsInGame().get(currentQuestion)).getVarThree();
        String currentQuestionVarFour = (game.getQuestionsInGame().get(currentQuestion)).getVarFour();
        int is5050Used = game.getIs5050Used();
        int isX2Used = game.getIsX2Used();
        int isCallUsed = game.getIsCallToFriendUsed();
        int isAudienceUsed = game.getIsAudienceHintUsed();
        int currentMoneyEarned = game.getCurrentMoneyAmountEarned();

        renderStableSumm(stableSumm);
        renderCurrentQuestionInTable(currentQuestion);
        renderQuestion(currentQuestionText);
        renderVarOne(currentQuestionVarOne);
        renderVarTwo(currentQuestionVarTwo);
        renderVarThree(currentQuestionVarThree);
        renderVarFour(currentQuestionVarFour);
        render5050(is5050Used);
        renderX2(isX2Used);
        renderCall(isCallUsed);
        renderAudience(isAudienceUsed);
        renderCurrentMoneyEarned(currentMoneyEarned);
        sharedPreferences.edit().putString("currentVarOne", currentQuestionVarOne).apply();
        sharedPreferences.edit().putString("currentVarTwo", currentQuestionVarTwo).apply();
        sharedPreferences.edit().putString("currentVarThree", currentQuestionVarThree).apply();
        sharedPreferences.edit().putString("currentVarFour", currentQuestionVarFour).apply();
    }

    private void renderCurrentMoneyEarned(int currentMoneyEarned){
        String currentMoneyEarnedString = String.valueOf(currentMoneyEarned);
        currentMoneyEarnedString = currentMoneyEarnedString + " Р";
        currentSumm.setText(currentMoneyEarnedString);
    }


    private void renderAudience(int isUsed){
        if(isUsed == 0) {
            audienceHelp.setVisibility(View.VISIBLE);
        }else {
            audienceHelp.setVisibility(View.GONE);}
    }

    private void renderCall(int isUsed){
        if(isUsed == 0) {
            callToFriend.setVisibility(View.VISIBLE);
        }else {
            callToFriend.setVisibility(View.GONE);}
    }

    private void render5050(int isUsed){
        if(isUsed == 0) {
            fiftyFifty.setVisibility(View.VISIBLE);
        }else {
            fiftyFifty.setVisibility(View.GONE);}
    }

    private void renderX2(int isUsed){
        if(isUsed == 0) {
            x2.setVisibility(View.VISIBLE);
        }else {
            x2.setVisibility(View.GONE);}
    }

    private void renderStableSumm(int stableSumm){
        switch (stableSumm){
            case 500:
                mark1Summ.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark1QuestionNumber.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark1Rhombus.setBackgroundResource(R.drawable.rhombus_small_stable);
                break;
            case 1000:
                mark2Summ.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark2QuestionNumber.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark2Rhombus.setBackgroundResource(R.drawable.rhombus_small_stable);
                break;
            case 2000:
                mark3Summ.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark3QuestionNumber.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark3Rhombus.setBackgroundResource(R.drawable.rhombus_small_stable);
                break;
            case 5000:
                mark4Summ.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark4QuestionNumber.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark4Rhombus.setBackgroundResource(R.drawable.rhombus_small_stable);
                break;
            case 10000:
                mark5Summ.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark5QuestionNumber.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark5Rhombus.setBackgroundResource(R.drawable.rhombus_small_stable);
                break;
            case 25000:
                mark6Summ.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark6QuestionNumber.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark6Rhombus.setBackgroundResource(R.drawable.rhombus_small_stable);
                break;
            case 50000:
                mark7Summ.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark7QuestionNumber.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark7Rhombus.setBackgroundResource(R.drawable.rhombus_small_stable);
                break;
            case 100000:
                mark8Summ.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark8QuestionNumber.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark8Rhombus.setBackgroundResource(R.drawable.rhombus_small_stable);
                break;
            case 200000:
                mark9Summ.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark9QuestionNumber.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark9Rhombus.setBackgroundResource(R.drawable.rhombus_small_stable);
                break;
            case 400000:
                mark10Summ.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark10QuestionNumber.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark10Rhombus.setBackgroundResource(R.drawable.rhombus_small_stable);
                break;
            case 800000:
                mark11Summ.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark11QuestionNumber.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark11Rhombus.setBackgroundResource(R.drawable.rhombus_small_stable);
                break;
            case 1500000:
                mark12Summ.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark12QuestionNumber.setTextColor(getResources().getColor(R.color.fbutton_color_emerald));
                mark12Rhombus.setBackgroundResource(R.drawable.rhombus_small_stable);
                break;
        }
        sharedPreferences.edit().putBoolean("isStableRendered",true).apply();
    }

    private void renderCurrentQuestionInTable(int currentQuestion){
        switch (currentQuestion){
            case 1:
                mark1.setBackgroundResource(R.drawable.rounded_shape_mark_current_question);
                break;
            case 2:
                mark1.setBackgroundResource(R.drawable.rounded_shape_mark);
                mark2.setBackgroundResource(R.drawable.rounded_shape_mark_current_question);
                break;
            case 3:
                mark2.setBackgroundResource(R.drawable.rounded_shape_mark);
                mark3.setBackgroundResource(R.drawable.rounded_shape_mark_current_question);
                break;
            case 4:
                mark3.setBackgroundResource(R.drawable.rounded_shape_mark);
                mark4.setBackgroundResource(R.drawable.rounded_shape_mark_current_question);
                break;
            case 5:
                mark4.setBackgroundResource(R.drawable.rounded_shape_mark);
                mark5.setBackgroundResource(R.drawable.rounded_shape_mark_current_question);
                break;
            case 6:
                mark5.setBackgroundResource(R.drawable.rounded_shape_mark);
                mark6.setBackgroundResource(R.drawable.rounded_shape_mark_current_question);
                break;
            case 7:
                mark6.setBackgroundResource(R.drawable.rounded_shape_mark);
                mark7.setBackgroundResource(R.drawable.rounded_shape_mark_current_question);
                break;
            case 8:
                mark7.setBackgroundResource(R.drawable.rounded_shape_mark);
                mark8.setBackgroundResource(R.drawable.rounded_shape_mark_current_question);
                break;
            case 9:
                mark8.setBackgroundResource(R.drawable.rounded_shape_mark);
                mark9.setBackgroundResource(R.drawable.rounded_shape_mark_current_question);
                break;
            case 10:
                mark9.setBackgroundResource(R.drawable.rounded_shape_mark);
                mark10.setBackgroundResource(R.drawable.rounded_shape_mark_current_question);
                break;
            case 11:
                mark10.setBackgroundResource(R.drawable.rounded_shape_mark);
                mark11.setBackgroundResource(R.drawable.rounded_shape_mark_current_question);
                break;
            case 12:
                mark11.setBackgroundResource(R.drawable.rounded_shape_mark);
                mark12.setBackgroundResource(R.drawable.rounded_shape_mark_current_question);
                break;
            case 13:
                mark12.setBackgroundResource(R.drawable.rounded_shape_mark);
                mark13.setBackgroundResource(R.drawable.rounded_shape_mark_current_question);
                break;
        }
    }

    private void renderQuestion(String currentQuestionText){
        questionText.setText(currentQuestionText);
        myDB = new DatabaseHelper(getContext());
        myDB.changeIsUsed(currentQuestionText);
        myDB.close();
    }

    private void renderVarOne(String currentQuestionVarOne){
        var1.setText(currentQuestionVarOne);
    }

    private void renderVarTwo(String currentQuestionVarTwo){
        var2.setText(currentQuestionVarTwo);
    }

    private void renderVarThree(String currentQuestionVarThree){
        var3.setText(currentQuestionVarThree);
    }

    private void renderVarFour(String currentQuestionVarFour){
        var4.setText(currentQuestionVarFour);
    }

    private Question getQuestion(int diffLvl) {
        Question question;
        myDB = new DatabaseHelper(getContext());
        c = myDB.getUniqueQuestionWithDiffLvl(diffLvl);
        int size = c.getCount();
        int i = 0;
        Question[] questions = new Question[size];
        try {
            if (c.moveToFirst()) {
                while (!c.isAfterLast()) {
                    questions[i] = new Question("", "", "", "", "", "", 1);
                    questions[i].setQuestionText(c.getString(c.getColumnIndex("questionText")));
                    questions[i].setQuestionCorrectAnswer(c.getString(c.getColumnIndex("questionCorrectAnswer")));
                    questions[i].setVarOne(c.getString(c.getColumnIndex("varOne")));
                    questions[i].setVarTwo(c.getString(c.getColumnIndex("varTwo")));
                    questions[i].setVarThree(c.getString(c.getColumnIndex("varThree")));
                    questions[i].setVarFour(c.getString(c.getColumnIndex("varFour")));
                    i++;
                    c.moveToNext();
                }
            }

            c.close();


        }catch (CursorIndexOutOfBoundsException e) {
            Log.e("LogError", "Error");
        }

        myDB.close();

        question = questions[(int)(System.currentTimeMillis() % questions.length)];

        return question;
    }

    private boolean doesStableSummHaveBeenReached(){
        int stableSumm = game.getStableSumm();
        return (getCurrentQuestionPrize(game.getCurrentQuestionNumber()) > stableSumm);
    }


    private void backToMenu(){
        FragmentManager fragmentManager;
        FragmentTransaction fragmentTransaction;
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .replace(R.id.fragment_container, new MenuFragment())
                .addToBackStack(null)
                .commit();
    }
}
