package com.example.wwtbm.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wwtbm.R;
import com.example.wwtbm.helpers.DatabaseHelper;
import com.example.wwtbm.models.User;


public class MenuFragment extends Fragment implements View.OnClickListener {


    private DatabaseHelper myDB;
    static final String myPreference = "myPref";
    private SharedPreferences sharedPreferences;
    private TextView userWalletTV, userLevelNumberTV, userLevelDescriptionTV;
    private ImageView userLevelIcon, walletBalanceIcon, chartIcon;
    private Cursor c;
    private User user;



    @Override
    @SuppressWarnings("all")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_menu, container, false);

        LinearLayout rateApplication;
        Button startButton, recordsButton, settingsButton, exitButton;

        userWalletTV = view.findViewById(R.id.user_wallet);
        userLevelNumberTV = view.findViewById(R.id.user_level_number);
        userLevelDescriptionTV = view.findViewById(R.id.level_description);
        userLevelIcon = view.findViewById(R.id.level_image_icon);
        rateApplication = view.findViewById(R.id.ll_rate_app);
        startButton = view.findViewById(R.id.button_start_play);
        recordsButton = view.findViewById(R.id.button_records);
        settingsButton = view.findViewById(R.id.button_settings);
        exitButton = view.findViewById(R.id.button_exit);
        walletBalanceIcon  = view.findViewById(R.id.ic_wallet_balance);
        chartIcon = view.findViewById(R.id.ic_chart);
        rateApplication.setOnClickListener(this);
        startButton.setOnClickListener(this);
        recordsButton.setOnClickListener(this);
        settingsButton.setOnClickListener(this);
        exitButton.setOnClickListener(this);

        myDB = new DatabaseHelper(getContext());
        sharedPreferences = getActivity().getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        if(isFirstLogin()){
            showAlertDialog();
        }else{
            initUser();
            attachDataToView(user);
        }

        return view;
    }



    public MenuFragment(){

    }


    /*MenuFragment(User user){
        this.user = user;
    }

     */


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_start_play:
                switchFragment(new GameFragment(user));
                break;
            case R.id.button_records:
                if(user.getGamesCompleted() == 0){
                    Toast.makeText(getContext(), "Необходимо сыграть хотя бы одну игру", Toast.LENGTH_SHORT).show();
                } else {
                    switchFragment(new RecordsFragment(user));
                }
                break;
            case R.id.button_settings:
                switchFragment(new SettingsFragment());
                break;
            case R.id.ll_rate_app:
                break;
            case R.id.button_exit:
                if(getActivity() != null) {
                        getActivity().finishAffinity();
                }


        }
    }

    private boolean isFirstLogin(){
        boolean isContainsUserName;
        isContainsUserName = sharedPreferences.contains("currentUserName");
        return !isContainsUserName;
    }

    @SuppressWarnings("all")
    private void switchFragment(Fragment fragment){
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction;
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    @SuppressWarnings("all")
    private void showAlertDialog(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        final View mView = getLayoutInflater().inflate(R.layout.dialog_user_name, null);
        final EditText userNameInput = mView.findViewById(R.id.user_name_input);
        if(!isFirstLogin()){
            userNameInput.setText(sharedPreferences.getString("currentUserName", ""));
        }
        alertDialog.setView(mView);

        if(isFirstLogin()) {
            alertDialog.setCancelable(false);
        } else{
            alertDialog.setCancelable(true);
        }

        alertDialog.setPositiveButton("Далее", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(isFirstLogin()){
                    String editTextValue = userNameInput.getText().toString();
                    if (!(editTextValue).equals("")) {
                       if(editTextValue.trim().length() < 3){
                            showAlertDialog();
                        }else{
                            String currentUserName = userNameInput.getText().toString();
                            currentUserName = currentUserName.trim();
                            if(getActivity() != null) {
                                getActivity().getSharedPreferences(myPreference, Context.MODE_PRIVATE).edit().putString("currentUserName", currentUserName).apply();
                            }
                            insertUser();
                            initUser();
                            attachDataToView(user);
                            dialog.dismiss();
                        }
                    } else {
                        showAlertDialog();
                    }
                }
            }

        });


        if (!isFirstLogin()) {
            alertDialog.setNegativeButton("Назад", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }

        AlertDialog dialog = alertDialog.create();
        if (isFirstLogin()) {
            dialog.setCanceledOnTouchOutside(false);
        } else {
            dialog.setCanceledOnTouchOutside(true);
        }
        dialog.show();

    }


    private void attachDataToView(User user){
        userWalletTV.setText(String.valueOf(user.getWalletBalance()));
        if(sharedPreferences.contains("isX2usedInLastRound"))
            sharedPreferences.edit().remove("isX2usedInLastRound").apply();
        if(!isFirstLogin()) {
            walletBalanceIcon.setVisibility(View.VISIBLE);
            chartIcon.setVisibility(View.VISIBLE);
        }

        switch (calculateUserLevel()) {
            case 1:
                userLevelDescriptionTV.setText("Полено");
                userLevelIcon.setImageResource(R.drawable.ic_log);
                break;
            case 2:
                userLevelDescriptionTV.setText("Воин");
                userLevelIcon.setImageResource(R.drawable.ic_sword);
                break;
            case 3:
                userLevelDescriptionTV.setText("Рыцарь");
                userLevelIcon.setImageResource(R.drawable.ic_knight);
                break;
            case 4:
                userLevelDescriptionTV.setText("Лучник");
                userLevelIcon.setImageResource(R.drawable.ic_archery);
                break;
            case 5:
                userLevelDescriptionTV.setText("Чемпион");
                userLevelIcon.setImageResource(R.drawable.ic_trophy);
                break;
            case 6:
                userLevelDescriptionTV.setText("Алмаз");
                userLevelIcon.setImageResource(R.drawable.ic_diamond);
                break;
            case 7:
                userLevelDescriptionTV.setText("Туз");
                userLevelIcon.setImageResource(R.drawable.ic_ace);
                break;
        }

        userLevelNumberTV.setText(String.valueOf(getColumnValue("levelNumber")));
    }


    private int calculateUserLevel(){
        int pointsNumber, gamesCompleted, gamesCompletedWithoutHints, walletBalance, userTotalPoints;
        pointsNumber = getColumnValue("points");
        gamesCompleted = getColumnValue("gamesCompleted");
        gamesCompletedWithoutHints = getColumnValue("gamesCompletedWithoutHints");
        walletBalance = getColumnValue("walletBalance");
        userTotalPoints = (int)Math.round(pointsNumber + 300 * gamesCompleted + 300 * gamesCompletedWithoutHints + 0.0001 * walletBalance);

        if(userTotalPoints>750 && userTotalPoints <= 3000){
            user.setLevelNumber(2);
            myDB.updateLevelNumber(user.getUserName(),2);
            return 2;
        }
        if(userTotalPoints>3000 && userTotalPoints <= 8000){
            user.setLevelNumber(3);
            myDB.updateLevelNumber(user.getUserName(),3);
            return 3;
        }
        if(userTotalPoints>8000 && userTotalPoints <=25000){
            user.setLevelNumber(4);
            myDB.updateLevelNumber(user.getUserName(),4);
            return 4;
        }
        if(userTotalPoints>25000 && userTotalPoints <= 55000){
            user.setLevelNumber(5);
            myDB.updateLevelNumber(user.getUserName(),5);
            return 5;
        }
        if(userTotalPoints>55000 && userTotalPoints <= 150000){
            user.setLevelNumber(6);
            myDB.updateLevelNumber(user.getUserName(),6);
            return 6;
        }
        if(userTotalPoints>150000){
            user.setLevelNumber(7);
            myDB.updateLevelNumber(user.getUserName(),7);
            return 7;
        }

        return 1;

    }

    private void insertUser(){
        myDB.insertUser(sharedPreferences.getString("currentUserName",""),0,0,0,0,0,0,0,0,0,1,0);
    }


    private int getColumnValue(String columnName){
        int value = -1;
        c = myDB.getUserInfo();
        if(c.moveToFirst()){
            do {
                value = c.getInt(c.getColumnIndex(columnName));
            }while(c.moveToNext());
        }
        c.close();

        return value;
    }

    private String getUsername() {
        String value = "";
        c = myDB.getUserInfo();
        if (c.moveToFirst()) {
            //do {
            value = c.getString(c.getColumnIndex("userName"));
            //} while (c.moveToNext());
        }
        c.close();

        return value;
    }

    private void initUser(){
        user = new User(sharedPreferences.getString("currentUserName",""),getColumnValue("walletBalance"),getColumnValue("gamesCompleted"), getColumnValue("gamesCompletedWithoutHints"),getColumnValue("maxRewardCount"),getColumnValue("longestQuestionSeriesWithoutHintsWithinOneGame"),getColumnValue("amountX2Used"), getColumnValue("amountFiftyFiftyUsed"), getColumnValue("amountCallUsed"),getColumnValue("amountAudienceUsed"), getColumnValue("levelNumber"), getColumnValue("points"));
    }



   /* private int getCurrentWalletBalance(){
        int value = -1;
        c = myDB.getUserInfo();
        if(c.moveToFirst()){
            //do {
                value = c.getInt(c.getColumnIndex("walletBalance"));
           // }while(c.moveToNext());
        }
        c.close();

        return value;
    }

    private int getCurrentLevelNumber(){
        int value = -1;
        c = myDB.getUserInfo();
        if(c.moveToFirst()){
            do {
                value = c.getInt(c.getColumnIndex("levelNumber"));
            }while(c.moveToNext());
        }
        c.close();

        return value;
    }





    private int getGamesCompleted(){
        int value = -1;
        c = myDB.getUserInfo();
        if(c.moveToFirst()){
            do {
                value = c.getInt(c.getColumnIndex("gamesCompleted"));
            }while(c.moveToNext());
        }
        c.close();

        return value;
    }

    private int getGamesCompletedWithoutHints(){
        int value = -1;
        c = myDB.getUserInfo();
        if(c.moveToFirst()){
            do {
                value = c.getInt(c.getColumnIndex("gamesCompletedWithoutHints"));
            }while(c.moveToNext());
        }
        c.close();

        return value;
    }


    private int getLongestQuestionSeriesWithoutHintsWithinOneGame(){
        int value = -1;
        c = myDB.getUserInfo();
        if(c.moveToFirst()){
            do{
                value = c.getInt(c.getColumnIndex("longestQuestionSeriesWithoutHintsWithinOneGame"));
            }while(c.moveToNext());
        }
        c.close();

        return value;
    }

    private int getAmountX2Used(){
        int value = -1;
        c = myDB.getUserInfo();
        if(c.moveToFirst()){
            do{
                value = c.getInt(c.getColumnIndex("amountX2Used"));
            }while(c.moveToNext());
        }
        c.close();

        return value;
    }

    private int getAmountFiftyFiftyUsed(){
        int value = -1;
        c = myDB.getUserInfo();
        if(c.moveToFirst()){
            do{
                value = c.getInt(c.getColumnIndex("amountFiftyFiftyUsed"));
            }while(c.moveToNext());
        }
        c.close();

        return value;
    }

    private int getAmountCallUsed(){
        int value = -1;
        c = myDB.getUserInfo();
        if(c.moveToFirst()){
            do{
                value = c.getInt(c.getColumnIndex("amountCallUsed"));
            }while(c.moveToNext());
        }
        c.close();

        return value;
    }

    private int getAmountAudienceUsed(){
        int value = -1;
        c = myDB.getUserInfo();
        if(c.moveToFirst()){
            do{
                value = c.getInt(c.getColumnIndex("amountAudienceUsed"));
            }while(c.moveToNext());
        }
        c.close();

        return value;
    }

    private int getPoints(){
        int value = -1;
        c = myDB.getUserInfo();
        if(c.moveToFirst()){
            do {
                value = c.getInt(c.getColumnIndex("points"));
            }while(c.moveToNext());
        }
        c.close();

        return value;
    }

    private int getMaxRewardCount(){
        int value = -1;
        c = myDB.getUserInfo();
        if(c.moveToFirst()){
            do{
                value = c.getInt(c.getColumnIndex("maxRewardCount"));
            }while(c.moveToNext());
        }
        c.close();

        return value;
    }



    */


}
