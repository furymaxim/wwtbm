package com.example.wwtbm.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wwtbm.R;
import com.example.wwtbm.helpers.DatabaseHelper;

import static com.example.wwtbm.fragments.MenuFragment.myPreference;

public class SettingsFragment extends Fragment {

    private SharedPreferences sharedPreferences;
    private DatabaseHelper myDB;
    private EditText editText;
    private CheckBox checkBox;
    private TextView confirmChanges;
    private boolean isDifferent;

    @Override
    @SuppressWarnings("all")
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        TextView back;

        isDifferent = false;

        myDB = new DatabaseHelper(getContext());

        sharedPreferences = getActivity().getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        editText = view.findViewById(R.id.new_username);
        checkBox = view.findViewById(R.id.checkbox);
        back = view.findViewById(R.id.back);
        confirmChanges = view.findViewById(R.id.confirm_changes);
        confirmChanges.setVisibility(View.INVISIBLE);



        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 2){
                    confirmChanges.setVisibility(View.VISIBLE);
                }else if(checkBox.isChecked()){
                    confirmChanges.setVisibility(View.VISIBLE);
                }else{
                    confirmChanges.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {

                if(isChecked){
                    confirmChanges.setVisibility(View.VISIBLE);
                }else if(editText.getText().length()> 2){
                    confirmChanges.setVisibility(View.VISIBLE);
                }else{
                    confirmChanges.setVisibility(View.INVISIBLE);
                }

        });


        confirmChanges.setOnClickListener((v) -> {

            if(checkBox.isChecked() && editText.getText().length() > 2) {
                if (!editText.getText().toString().equals(sharedPreferences.getString("currentUserName", ""))) {
                    isDifferent = true;
                    dialogConfirmReset();

                }else{
                    Toast.makeText(getContext(),"Новое имя совпадает с текущим. Введи другое имя.",Toast.LENGTH_SHORT).show();
                    editText.setText("");
                }
            }else{
                checkFields();
            }


        });

        back.setOnClickListener((v)-> backToMenu());

        return  view;
    }


    private void checkFields(){

/*        if(checkBox.isChecked() && editText.getText().length() > 2){
            if(!editText.getText().toString().equals(sharedPreferences.getString("currentUserName",""))) {
                isDifferent = true;
               // myDB.deleteGamesFromHistory();
               // sharedPreferences.edit().remove("lastGameId").apply();

               // myDB.deleteUser(sharedPreferences.getString("currentUserName", ""));
               // myDB.insertUser(sharedPreferences.getString("currentUserName", ""), 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0);
            }else{
                editText.setText("");
                checkBox.setChecked(false);
                Toast.makeText(getContext(),"Новое имя совпадает с текущим. Введи другое имя.",Toast.LENGTH_SHORT).show();
                isDifferent = false;
            }
        }*/

        if(checkBox.isChecked()){
            //isDifferent = true;
            isDifferent = false;
            dialogConfirmReset();
            //  myDB.deleteGamesFromHistory();
            // sharedPreferences.edit().remove("lastGameId").apply();
            //myDB.deleteUser(sharedPreferences.getString("currentUserName",""));
            //myDB.insertUser(sharedPreferences.getString("currentUserName",""),0,0,0,0,0,0,0,0,0,1,0);
        }

        if(editText.getText().length() > 2){
            if(!editText.getText().toString().equals(sharedPreferences.getString("currentUserName",""))) {
                isDifferent = true;
                myDB.updateUsername(editText.getText().toString());
                sharedPreferences.edit().putString("currentUserName", editText.getText().toString()).apply();
                backToMenu();
                Toast.makeText(getContext(), "Изменения успешны применены", Toast.LENGTH_SHORT).show();
            }else{
                editText.setText("");
                Toast.makeText(getContext(),"Новое имя совпадает с текущим. Введи другое имя.",Toast.LENGTH_SHORT).show();
            }
        }

        if(isDifferent) {
            backToMenu();

            confirmChanges.setVisibility(View.INVISIBLE);

            Toast.makeText(getContext(), "Изменения успешны применены", Toast.LENGTH_SHORT).show();
        }



        myDB.close();
    }

    private void dialogConfirmReset() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(false);
        final View mView = getLayoutInflater().inflate(R.layout.dialog_get_money, null);
        final TextView moneyTV = mView.findViewById(R.id.text_money_amount);
        moneyTV.setText(sharedPreferences.getString("currentUserName", "") + ", действительно хочешь обнулить прогресс?");
        alertDialog.setView(mView);
        alertDialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(isDifferent){
                    myDB.updateUsername(editText.getText().toString());
                    sharedPreferences.edit().putString("currentUserName", editText.getText().toString()).apply();
                }
                myDB.deleteGamesFromHistory();
                sharedPreferences.edit().remove("lastGameId").apply();
                myDB.deleteUser(sharedPreferences.getString("currentUserName",""));
                myDB.insertUser(sharedPreferences.getString("currentUserName",""),0,0,0,0,0,0,0,0,0,1,0);
                backToMenu();
                Toast.makeText(getContext(), "Изменения успешны применены", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }

        });
        alertDialog.setNegativeButton("Нет, не хочу", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkBox.setChecked(false);
                dialog.dismiss();
            }
        });

        AlertDialog dialog = alertDialog.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @SuppressWarnings("all")
    private void backToMenu(){
        FragmentManager fragmentManager;
        FragmentTransaction fragmentTransaction;
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .replace(R.id.fragment_container, new MenuFragment())
                .addToBackStack(null)
                .commit();
    }






}
