package com.example.wwtbm.fragments;


import android.database.Cursor;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.wwtbm.R;
import com.example.wwtbm.helpers.DatabaseHelper;
import com.example.wwtbm.models.GameHistory;
import com.example.wwtbm.models.User;

public class RecordsFragment extends Fragment {

    private User user;
    private GameHistory gameHistory;
    private String userName = "";
    private int gameID = 0;
    private String gameDate = "";
    private int gamePrizeAmount = 0;
    private int amountOfHintsUsed = 0;
    private int stableSumm = 0;
    private int x2used = 0;
    private int fiftyFiftyUsed = 0;
    private int callused = 0;
    private int audienceused = 0;
    private int questionNumber = 0;
    private String lastQuestionText = "";
    private int lastQuestionAnswerGivenOrNot = 0;
    private String userAnswer= "";
    private String correctAnswer = "";
    private TextView shortInfo;


    RecordsFragment(User user) {
        this.user = user;
    }


    @Override
    @SuppressWarnings("all")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_records, container, false);
        TextView back, gamesCompleted, gamesCompletedWithoutHints, maxRewardCount, longestSeries, x2Used, fiftyUsed, callUsed, audienceUsed, levelNumber;
        TextView fullInfo;

        back = view.findViewById(R.id.back1);
        gamesCompleted = view.findViewById(R.id.gamesCompleted);
        gamesCompletedWithoutHints = view.findViewById(R.id.gamesCompletedWithoutHints);
        maxRewardCount = view.findViewById(R.id.maxRewardCount);
        longestSeries = view.findViewById(R.id.longestQuestionSeriesWithoutHintsWithinOneGame);
        x2Used = view.findViewById(R.id.x2);
        fiftyUsed = view.findViewById(R.id.fifty);
        callUsed = view.findViewById(R.id.call);
        audienceUsed = view.findViewById(R.id.audience_help);
        levelNumber = view.findViewById(R.id.lvlNumber);
        shortInfo = view.findViewById(R.id.shortInfo);
        fullInfo = view.findViewById(R.id.fullInfo);

        gamesCompleted.setText(gamesCompleted.getText().toString() + " " + user.getGamesCompleted());
        gamesCompletedWithoutHints.setText(gamesCompletedWithoutHints.getText().toString() + " " + user.getGamesCompletedWithoutHints());
        maxRewardCount.setText(maxRewardCount.getText().toString() + " " + user.getMaxRewardCount());
        longestSeries.setText(longestSeries.getText().toString() + " " + user.getLongestQuestionSeriesWithoutHintsWithinOneGame());
        x2Used.setText(x2Used.getText().toString() + " " + user.getAmountX2Used());
        fiftyUsed.setText(fiftyUsed.getText().toString() + " " + user.getAmountFiftyFiftyUsed());
        callUsed.setText(callUsed.getText().toString() + " " +user.getAmountCallUsed());
        audienceUsed.setText(audienceUsed.getText().toString() + " " + user.getAmountAudienceUsed());
        levelNumber.setText(levelNumber.getText().toString() + " " + user.getLevelNumber());

        formRecords();

        initGameHistory();

        formShortInfo();


        //tvInfo.setText(userName + " " + gameID + " " + gameDate + " " + gamePrizeAmount + " " + amountOfHintsUsed + " " + stableSumm + " " + x2Used + " " + fiftyFiftyUsed + " " + callUsed + " " + audienceUsed + " " + questionNumber + " " + lastQuestionText + " " + lastQuestionAnswerGivenOrNot + " " + userAnswer + " " + correctAnswer);

        fullInfo.setOnClickListener(v -> showInfoAboutGame());

        back.setOnClickListener(v -> backToMenu());

        return view;

    }


    private void initGameHistory(){
        gameHistory = new GameHistory(userName, gameID, gameDate, gamePrizeAmount, amountOfHintsUsed, stableSumm, x2used, fiftyFiftyUsed, callused, audienceused, questionNumber, lastQuestionText, lastQuestionAnswerGivenOrNot, userAnswer, correctAnswer);
    }

    private void formShortInfo(){
        shortInfo.setText("Дата: " + gameHistory.getGameDate() + "\nВыигрыш: " + gameHistory.getGamePrizeAmount() + "\nПодсказок использовано: " + gameHistory.getAmountOfHintsUsed());
    }


    private void formRecords(){
        DatabaseHelper myDB = new DatabaseHelper(getContext());
        Cursor c;
        c = myDB.getGameWithTheHighestPrizeAmount();
        if(c.moveToFirst()){
            //  do{
            userName = c.getString(c.getColumnIndex("userName"));
            gameID = c.getInt(c.getColumnIndex("gameID"));
            gameDate = c.getString(c.getColumnIndex("gameDate"));
            gamePrizeAmount = c.getInt(c.getColumnIndex("gamePrizeAmount"));
            amountOfHintsUsed = c.getInt(c.getColumnIndex("amountOfHintsUsed"));
            stableSumm = c.getInt(c.getColumnIndex("stableSumm"));
            x2used = c.getInt(c.getColumnIndex("x2Used"));
            fiftyFiftyUsed = c.getInt(c.getColumnIndex("fiftyFiftyUsed"));
            callused = c.getInt(c.getColumnIndex("callUsed"));
            audienceused = c.getInt(c.getColumnIndex("audienceUsed"));
            questionNumber = c.getInt(c.getColumnIndex("questionNumber"));
            lastQuestionText = c.getString(c.getColumnIndex("lastQuestionText"));
            lastQuestionAnswerGivenOrNot = c.getInt(c.getColumnIndex("lastQuestionAnswerGivenOrNot"));
            userAnswer = c.getString(c.getColumnIndex("userAnswer"));
            correctAnswer = c.getString(c.getColumnIndex("correctAnswer"));
            // }while(c.moveToNext());
        }


        c.close();
    }

    @SuppressWarnings("all")
    private void showInfoAboutGame(){
        String currentText;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(true);
        final View mView = getLayoutInflater().inflate(R.layout.dialog_full_info, null);
        TextView fullInfo = mView.findViewById(R.id.full_tv);
        final TextView fullInfo2 = mView.findViewById(R.id.full_tv_2);
        final LinearLayout call = mView.findViewById(R.id.call_to_friend_ll);
        final LinearLayout audience = mView.findViewById(R.id.audience_help_ll);
        final TextView x2 = mView.findViewById(R.id.x2_ll);
        final TextView fifty = mView.findViewById(R.id.fifty_fifty_ll);
        alertDialog.setView(mView);


        fullInfo.setText("ID игры: " + gameHistory.getGameID() + "\n" + "Дата игры: " + gameHistory.getGameDate() + "\n" + "Выигранная сумма: " + gameHistory.getGamePrizeAmount() + "\n" + "Подсказок использовано: " + gameHistory.getAmountOfHintsUsed() + "\n" + "Несгораемая сумма: " + gameHistory.getStableSumm());
        currentText = fullInfo.getText().toString();
        if(gameHistory.getAmountOfHintsUsed() != 0){
            fullInfo.setText(currentText + "\nИспользованные подсказки: ");
            if(gameHistory.getX2Used() == 1){
                x2.setVisibility(View.VISIBLE);
            }
            if(gameHistory.getFiftyFiftyUsed() == 1){
                fifty.setVisibility(View.VISIBLE);
            }
            if(gameHistory.getCallUsed() == 1){
                call.setVisibility(View.VISIBLE);
            }
            if(gameHistory.getAudienceUsed() == 1){
                audience.setVisibility(View.VISIBLE);
            }

        }

        fullInfo2.setText("Номер последнего вопроса: " + gameHistory.getQuestionNumber() + "\n" + "Текст последнего вопроса:\n" + gameHistory.getLastQuestionText() + "\n" + "Был дан ответ: ") ;
        currentText = fullInfo2.getText().toString();
        if(gameHistory.getLastQuestionAnswerGivenOrNot() == 1){
            fullInfo2.setText(currentText + "Да\n" + "Твой ответ: " + gameHistory.getUserAnswer());
            if(gameHistory.getQuestionNumber() != 13){
                currentText = fullInfo2.getText().toString();
                fullInfo2.setText(currentText + "\nПравильный ответ: " + gameHistory.getCorrectAnswer());
            }


        }else{
            fullInfo2.setText(currentText + "Нет\n");
            currentText = fullInfo2.getText().toString();
            fullInfo2.setText(currentText + "Правильный ответ: " + gameHistory.getCorrectAnswer());
        }


        AlertDialog dialog = alertDialog.create();
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }


    @SuppressWarnings("all")
    private void backToMenu(){
        FragmentManager fragmentManager;
        FragmentTransaction fragmentTransaction;
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .replace(R.id.fragment_container, new MenuFragment())
                .addToBackStack(null)
                .commit();
    }

}
